import React from 'react'

function Somos() {
    return (
        <div style={{ maxWidth: '500px', margin: 'auto', padding: '1rem' }}>
            <h1>Términos y Condiciones</h1>
            <h2>Propósito</h2>
            <p>Este sitio web ha sido creado para promocionar alimentos y bebidas, cuyo objetivo es proporcionar documentos legales estéticamente agradables para los negocios.</p>

            <h2>Condiciones Generales</h2>
            <p>
                En la medida permitida por la ley, de ninguna manera seremos responsables ante usted o cualquier otra persona por cualquier pérdida o daño, independientemente de cómo sea causado (ya sea directo, indirecto, consecuente o económico) que pueda sufrir directa o indirectamente en relación con el uso de este sitio web o sitios web de otras entidades que están hipervinculados de este sitio web (sitios web vinculados).
                Estas condiciones generales no están restringidas ni modificadas por ninguna de las siguientes advertencias y renuncias de responsabilidad específicas.
            </p>
            
            <h2>Proteccion de información personal</h2>
            <p>
                Para proteger su información personal y financiera, nuestro sitio utiliza formularios de respuesta encriptados de seguridad cuando se solicitan detalles personales y financieros. La seguridad que utilizamos entre su navegador y nuestro sitio web es el cifrado TLS de 256 bits.
                Para verificar la seguridad de nuestras páginas web dentro de su navegador, haga clic en el icono de candado bloqueado ubicado cerca de la dirección URL de la página web. Haga doble clic en el icono del candado. Dependiendo de su navegador, verá los detalles del certificado, como a quién se lo expidió, quién lo emitió y el período de validez del certificado.
                Estas medidas de seguridad se revisan periódicamente de conformidad con el Estándar de seguridad de datos de la industria de tarjetas de pago.
            </p>

            <h2>Uso de este sitio web</h2>
            <p>Como condición para el uso de este sitio web, nos garantiza que no usará este sitio web para ningún fin que esté prohibido por estos Términos de uso. En particular, usted acepta no:
                Use este sitio web para difamar, abusar, acosar, acechar, amenazar u ofender a cualquier persona;
                Publicar, distribuir, enviar por correo electrónico, transmitir o difundir cualquier material que sea ilegal, obsceno, difamatorio, indecente, ofensivo o inapropiado;
                Use cualquier herramienta o software automatizado de secuencias de comandos;
                Participar o promover encuestas de terceros, concursos, esquemas piramidales, cadenas de cartas, correos electrónicos no solicitados o spam a través de este sitio web;
                Hacerse pasar por cualquier otra persona o entidad;
                Compartir información que identifique razonablemente a otra persona sin su consentimiento (si comparte información personal sobre otra persona, infórmeles sobre nuestro Aviso de recopilación de privacidad y nuestra Política de privacidad que establece cómo manejaremos su información personal);
                Cargue, publique, envíe por correo electrónico, transmita o ponga a disposición de cualquier otro modo mediante este sitio web cualquier material que no tenga derecho a poner a disposición o que contenga virus u otros códigos, archivos o programas informáticos diseñados para interrumpir, limitar o destruir la funcionalidad de otros software o hardware informático o para utilizar otros sistemas informáticos con fines inadecuados o sin autorización; o
                Incumplir las leyes o regulaciones que sean aplicables al uso de este sitio web en su jurisdicción.
                Si tiene una queja sobre el contenido que puede publicarse en este sitio web, debe comunicarse con nosotros.
            </p>

            <h2>Marcas y nombres comerciales</h2>
            <p>
                Todos los nombres comerciales, marcas comerciales, marcas de servicio y otros nombres y logotipos de productos y servicios ("Marcas") que se muestran en este sitio web son propiedad de sus respectivos dueños y están protegidos por las leyes de marca registrada y derechos de autor aplicables. Estas Marcas pueden ser nuestras Marcas registradas o no registradas o pueden pertenecer a otros y se utilizan en este sitio web con el permiso del propietario correspondiente.
                Nada de lo contenido en este sitio web debe interpretarse como una concesión de licencia o derecho de uso de cualquier marca que se muestre en este sitio web sin el permiso expreso por escrito del propietario correspondiente.
                Si utiliza cualquiera de nuestras Marcas para referirse a nuestras actividades, productos o servicios, debe incluir una declaración que nos atribuya esa Marca: en o como la totalidad o parte de sus propias Marcas;
                en relación con actividades, productos o servicios que no son nuestros;
                de una manera que puede ser confusa, engañosa o engañosa; o
                de una manera que nos menosprecia a nosotros o nuestra información, productos o servicios (incluido este sitio web).
            </p>

            <h2>Su visita a nuestro sitio web</h2>
            <p>
                Utilizamos cookies (pequeños archivos de datos que se almacenan en su computadora) para recopilar datos de tráfico anónimos y mejorar la experiencia de usuario de su sitio web. Puede eliminar o bloquear las cookies utilizando la configuración de su navegador web, pero esto puede afectar su capacidad para utilizar el sitio web.
                También podemos recopilar y almacenar información sobre su visita al sitio web, que incluye:
                el nombre del dominio desde el que accedió a internet;
                el tipo de dispositivo desde el que accedió al sitio web; y
                la ubicación desde la que accedió al sitio web.
                Toda la información que recopilamos se agrega y no se puede utilizar para identificarlo individualmente. Esta información se utiliza para medir el número de visitantes y evaluar cómo se utilizó el sitio web para mantener su eficacia.
                No recopilamos esta información para ningún tipo de publicidad en línea.
            </p>

            <h2>Redes sociales</h2>
            <p>
                Tenemos varias cuentas de redes sociales, que actualmente incluyen Facebook, Twitter, YouTube, Instagram y LinkedIn (conocidas colectivamente como nuestras Páginas). Todo el contenido de nuestras páginas está sujeto a los términos de uso individuales tal como lo describe cada proveedor de redes sociales.
                Al interactuar con nuestras páginas, usted acepta estar sujeto a los términos de uso del proveedor de redes sociales relevante.
            </p>

            <h2>Contacto</h2>
            <p>Mientras no poseemos una locación física, sí estamos presente virtualmente:</p>
            <p>hola@comidagodin.com</p>
        </div>
    )
}

export default Somos
