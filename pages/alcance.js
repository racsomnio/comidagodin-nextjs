import OnlyIfLogin from '../components/common/OnlyIfLogin'
import { withApollo } from '../utils/apollo'
import ShowALlStores from '../components/store/ShowAllStores'

function Alcance() {
    return (
        <OnlyIfLogin>
            <ShowALlStores/>
        </OnlyIfLogin>
    )
}

export default withApollo()(Alcance)
