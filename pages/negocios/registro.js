import { useState, useEffect } from 'react'
import Router from 'next/router'
import { withApollo } from "../../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import {CREATE_STORE, GET_CURRENT_USER} from '../../queries';
import styled from 'styled-components'
import PasswordEye from '../../components/form/PasswordEye'
import Error from '../../components/common/Error'
import Tooltip from '../../components/common/Tooltip'
import { string, object, array } from 'yup';
import dynamic from 'next/dynamic'
const PlaceAutocomplete = dynamic(
    () => import('../../components/common/PlaceAutocomplete'),
    { ssr: false }
) //Made it dynamic so it doesnt break due to waiting for js


const initialState = {
    name: '',
    typeOfBusiness: [],
    phone: [{number: ''}],
    location: {
        address: '',
        coordinates: [0,0]
    },
    email: '',
    password: ''
}

const validationSchema = object().shape({
    // typeOfBusiness: string().required('Selecciona tu Tipo de Establecimiento'),
    password: string().min(6, 'Mínimo 6 letras, números ó caracteres.').required('Necesitas una contraseña'),
    email: string().email('Este correo no es válido').required('Tu correo es necesario'),
    location: object().shape({ address: string().required('Tu Dirección es necesaria') }),
    phone: array().of(object().shape({ number: string().length(10, 'Pon un teléfono que sea válido') })),
    name: string().required('Un nombre es necesario'),
});

const Registro = () => {
    const [formValues, setFormValues] = useState(initialState);
    const [ errors, setErrors ] = useState({});

    function handleChange(e) {
        e.persist();
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    async function validateInputs (e) {
        const { name, value } = e.target;
        await validationSchema.validateAt(name, formValues).then(valid => {
            // console.log('valid')
            }).catch(err => {
            console.log('err:', err)
        });
    }

    async function validateAllInputs (e) {
        e.preventDefault();
        await validationSchema.validate(
            formValues,
            // { abortEarly: false }
        ).then(valid => {
            console.log('valid');
            setErrors({})
            handleSubmit(e);
          }).catch(err => {
            console.log('err:', err)
            setErrors(err)
        })
    }

    function handleClickBusiness(e) {
        e.persist();
        const { value } = e.target.previousSibling;
        const isThere = formValues.typeOfBusiness.includes(value);

        isThere 
            ? setFormValues(prevState => ({
                ...prevState,
                typeOfBusiness: prevState.typeOfBusiness.filter( b => b !== value)
            }))
            : setFormValues(prevState => ({
                ...prevState,
                typeOfBusiness: [...prevState.typeOfBusiness, value]
            }))
    }

    function handlePhone(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            phone: [{number: value}]
        }));
    }

    function addLocation(location) {
        setFormValues(prevState =>({
            ...prevState,
            location
        }));
    }

    function handleSubmit(e) {
        createStore().then(async({data}) => {
            clearState();
            Router.push(`${data.createStore.__typename == 'Store'? '/negocios/perfil' : '/'}`); 
        });
    }

    function clearState() {
        setFormValues({...initialState});
    }

    const [createStore, {error, loading}] = useMutation(CREATE_STORE, {
        variables: formValues,
        refetchQueries: [{query: GET_CURRENT_USER}],
        // awaitRefetchQueries: true,
    });

    const businesses = [
        "restaurante",
        "bistro",
        "fonda",
        "food-truck",
        "local",
        "puesto",
        "taquería",
        "comida-rápida",
        "pizzería",
        "hamburguesería",
        "marisquería",
        "panadería",
        "repostería",
        "cafetería",
        "casa-de-té",
        "heladería",
        "bar",
        "antro",
        "casino",
        "billar",
        "otro",
    ]

    const BusinessList = businesses.map( business => {
        const businessName = business.replace(/-/g, " ");
        return <div key={business}>
                <input type="checkbox" id={business} value={business} name="typeOfBusiness"></input>
                <label htmlFor={business} onClick={handleClickBusiness}> {businessName}</label>
            </div>
    });

    return (
        <SignupWrap>
            <h1>Regístra tu Negocio</h1>

            <p>
                Inténtalo GRATIS
            </p>

            <Form onSubmit={validateAllInputs}>
                <Fields>
                    <div className="field">
                        
                        <Label>Nombre de tu negocio</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-name">Verifica</strong>,". Este nombre no se puede cambiar en el futuro."]}
                            error={errors.path === 'name' && errors.errors[0]}
                        >
                            <input type="text" name="name" placeholder="Ej: Los Azules" onChange={handleChange} onBlur={validateInputs} value={formValues.name} />
                        </Tooltip>
                    </div>

                    <div className="field">
                        <Label>Teléfono</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-phone">Verifica</strong>,". Teléfono para información y/o pedidos."]}
                            error={errors.path === 'phone[0].number' && errors.errors[0]}
                        >
                            <input type="tel" name="phone" placeholder="Teléfono del Negocio" onChange={handlePhone} onBlur={validateInputs} value={formValues.phone[0].number} />
                        </Tooltip>
                    </div>

                    <div className="field full">
                        <Label>Escribe y selecciona la dirección de tu negocio</Label>
                        <Tooltip 
                            showOnHover={false}
                            error={errors.path === 'location.address' && errors.errors[0]}
                        >
                            <PlaceAutocomplete addLocationToParentState={addLocation} />
                        </Tooltip>
                    </div>

                    <div className="field full">
                        <Label>Selecciona categoría(s)</Label>
                        <CheckboxList>
                            {BusinessList}
                        </CheckboxList>
                    </div>

                    <div className="field">
                        <Label>Correo Electrónico</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message="Este correo no será visible"
                            error={errors.path === 'email' && errors.errors[0]}
                        >
                            <input type="email" name="email" placeholder="Email" onChange={handleChange} onBlur={validateInputs} value={formValues.email} />
                        </Tooltip>
                    </div>

                    <div className="field">
                        <Label>Crea una Contraseña</Label>
                        <Tooltip 
                            showOnHover={false}
                            showOnFocus
                            message={[<strong key="tt-password">Verifica</strong>,". Mínimo 6 letras, números ó caracteres."]}
                            error={errors.path === 'password' && errors.errors[0]}
                        >
                            <PasswordEye 
                                name="password" 
                                placeholder="Contraseña" 
                                onChange={handleChange} 
                                value={formValues.password} 
                                onBlur={validateInputs}
                            />
                        </Tooltip>
                    </div>
                </Fields>

                <button 
                    className="ui button" 
                    type="submit"
                    disabled={loading}
                >
                    Enviar
                </button>
                { error && <Error error={error}/> }
            </Form>
        </SignupWrap>
    )
}

const SignupWrap = styled.div`
    padding: 0 1rem;
`;

const Form = styled.form`
    background: #fff;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    padding: 1rem 1rem 3rem;
    max-width: 700px;
    margin: auto;
`;

const CheckboxList = styled.div`
    font-size: 0.8rem;
    display: flex;
    flex-wrap: wrap;
    margin-bottom: 1rem;
    padding-bottom: 2rem;
    border-bottom: 1px solid #e6e7e8;

    input[type="checkbox"] {
        display:none;
        width: 0;
        height: 0;

        & + label {
           background: #fff;
           padding: 0.5rem;
           border-radius: 25px;
           border: 1px solid #e6e7e8;
           display: block;
           margin: 0.4rem;
           text-transform: capitalize;
           font-weight: bold;
           color: #999;
           cursor: pointer;
        }

        &:checked + label {
            background: #7a1a7b;
            color: #fff;
        }
    }
`;

const Label = styled.label`
    display: block;
    font-size: 0.8rem;
    margin-bottom:0.2rem;
    font-weight: bold;
`;

const Fields = styled.div`
    display: flex;
    flex-wrap: wrap;

    input {
        width: 100%;
    }

    .field {
        flex: 1 1 320px;
        min-height: 130px;
        padding: 1rem;

        &.full {
            flex: 1 1 100%;
        }
    }
`;

export default withApollo({ ssr: true })(Registro);
