import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import styled from 'styled-components'
import { useMutation, useQuery } from '@apollo/react-hooks';
import { withApollo } from '../../utils/apollo'
import { CREATE_STRIPE_SUBSCRIPTION, GET_MY_STORE, GET_CURRENT_USER, GET_STORE_BY_SLUG } from '../../queries';
import Loading from '../../components/common/Loading'
import Error from '../../components/common/Error'

const Pagos = () => {
    const router = useRouter();
    const { session_id } = router.query;
    const [email, setEmail] = useState("");
    const {loading: userLoading, error: userError, data: userData} = useQuery(GET_CURRENT_USER);
    const [createStripeSubscription, {error, loading}] = useMutation(CREATE_STRIPE_SUBSCRIPTION, {
        variables: { stripeSessionId: session_id },
        refetchQueries: [{query: GET_MY_STORE}, {query: GET_CURRENT_USER}, 
            {
                query: GET_STORE_BY_SLUG, 
                variables: {
                    skip: !userData || !userData.getCurrentUser ,
                    slug: (userData && userData.getCurrentUser) && userData.getCurrentUser.slug
                }
            }]
    })

    useEffect(() => {
        createStripeSubs()
    }, [session_id, userData])

    async function createStripeSubs() {
        if(userData){
            const res = await createStripeSubscription();
            if(res.data.createStripeSubscription.email) setEmail(res.data.createStripeSubscription.email)
        }
    }

    if( userLoading ) return <Loading />
    if( userError ) return <Error error={error} />

    return (
        <div>
            <h1>Confirmación</h1>
            {
                email &&
                <BoxInfo>
                    <p>Tu pago se ha procesado con éxito.</p>
                    <p>Te hemos enviado tu recibo a: <strong>{email}</strong></p>
                    <p>Para futuras referencias sólo necesitarás proveer tu correo.</p>

                    <div style={{ marginBottom: '1rem' }}>
                        <Link href="/negocios/mi-menu" >
                            <a className="button secondary">Continuar Editando Menú</a>
                        </Link>
                    </div>

                    <div>
                        <Link href="/negocios/personalizar-menu" >
                            <a className="button">Personaliza tu Menú</a>
                        </Link>
                    </div>
                </BoxInfo>

                
            }

            {
                error && 
                <div>
                    <Error error={error} />
                    <p>Tu pago no se ha procesado, si crees que algo salió mal no dudes en contactarnos.</p>
                </div>
            }
            
        </div>
    )
}

export default withApollo({ssr: true})(Pagos)

const BoxInfo = styled.div`
    background: #fff;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    padding: 1rem 2rem 2rem;
    max-width: 400px;
    margin: auto;
`;
