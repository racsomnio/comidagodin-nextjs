import styled from 'styled-components'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import OnlyIfLogin from '../../components/common/OnlyIfLogin'
import { withApollo } from '../../utils/apollo'
import Upgrade from '../../components/common/Upgrade'
const CreateMenu = dynamic(
    () => import('../../components/menu/CreateMenu'),
    { ssr: false }
)


function AddMenu() {
    return (
        <OnlyIfLogin>
            <h1>Agrega tu menú</h1>
            <Grid>
                <PlanGold>
                    <strong>¿No tienes tiempo?</strong><br/>
                    <span>Mándanos una foto de tu menú y nosotros:</span>
                  
                    <ol>
                        <li>Lo subimos con imágenes,</li>
                        <li>Galería de fotos,</li>
                        <li>Código QR,</li>
                        <li>Traducción en inglés,</li>
                        <li>y libre de publicidad.</li>
                    </ol> 

                    <Link href="/negocios/ver-planes">
                        <a className="button secondary">Ver Ejemplos</a>
                    </Link>
                    
                </PlanGold>
                <CreateMenu/>
            </Grid>
        </OnlyIfLogin>
    )
}

export default withApollo()(AddMenu)

const PlanGold = styled.div`
    max-width: 320px;
    background: #fff;
    border: 1px solid #e6e7e8;
    border-radius: 10px;
    margin-top: 1rem;
    margin-bottom: 2rem;
    font-size: 0.8rem;

    strong {
        display: block;
        padding: 1rem 0;
        background: #fbdb6d;
        border-radius: 10px 10px 0 0;
    }

    span { padding: 0 1rem;}

    ol {
        text-align: left;
        margin-bottom: 1.5rem;
    }
`;

const Grid = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
`;
