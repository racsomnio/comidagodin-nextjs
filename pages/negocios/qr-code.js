import QRCode from 'qrcode.react'
import styled from 'styled-components'
import { withApollo } from "../../utils/apollo";
import { useQuery } from "@apollo/react-hooks"
import {GET_CURRENT_USER} from '../../queries';
import OnlyIfLogin from '../../components/common/OnlyIfLogin'
import Loading from '../../components/common/Loading'
import Error from '../../components/common/Error'

const QrPage = () => {
    const { data, loading, error} = useQuery(GET_CURRENT_USER);

    if(loading) return <Loading/>
    if(error) return <Error error={error}/>

    const { slug, _id } = data.getCurrentUser;

    function downloadQR(){
        const canvas = document.getElementById(_id);
        const pngUrl = canvas
            .toDataURL("image/png")
            .replace("image/png", "image/octet-stream");
        let downloadLink = document.createElement("a");
        downloadLink.href = pngUrl;
        downloadLink.download = `${slug}-QR.png`;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }

    return (
        <OnlyIfLogin>
            <div style={{ marginBottom: '-25px', zIndex: '0', position: 'relative'}}><strong>MENÚ DIGITAL</strong></div>
            <QRCode
                id={_id}
                value={`https://comidagodin.com/${slug}`}
                size={320}
                level={"M"}
                includeMargin={true}
            />
            <div style={{ fontSize: '0.75rem', marginTop: '-25px', zIndex: '0', position: 'relative', marginBottom: '1rem'}}>{`comidagodin.com/${slug}`}</div>
            <Buttons>
                <a className="button" onClick={downloadQR}> Descargar</a>
                <a className="button secondary" onClick={() => window.print()}> Imprimir</a>
            </Buttons>

            <QRinst>
                <p>Haz una prueba con tu teléfono</p>
                <ol>
                    <li>Activa la cámara de tu celular</li> 
                    <li>Sin tomar una foto, apunta tu cámara hacia el código de arriba.</li>
                    <li>¿Te apareció un mensaje con un enlace a comidagodin.com/...?</li>
                    <li>Si es así, da clic en el enlace y se abrirá tu menú digital</li>
                </ol>

                <p>¿No te apareció el mensaje?</p>
                <ol>
                    <li>¿Te aparecío un ícono amarillo como este? <img src="/icons/yellow-scan.svg" alt="qr code"/></li> 
                    <li>Si lo puedes ver, da clic en él.</li>
                    <li>Te aparecerá el enlance a comidagodin.com</li>
                </ol>

                <p>¿No te apareció el ícono amarillo?</p>
                <ol>
                    <li>¿Cuándo activas tu cámara puedes ver este ícono? <img src="/icons/glens.svg" alt="google lens"/></li> 
                    <li>Si lo puedes ver, da clic en él. Se abrirá Google Lens</li>
                    <li>Apunta de nuevo tu teléfono hacia el código de arriba y da clic en la lupa</li>
                    <li>Te aparecerá el enlace de comidagodín.com.</li>
                </ol>

                <p>¿No ves el ícono?</p>
                <ol>
                    <li>Ve a la configuración de tu camera. Puedes acceder dando clic a la tuerca que aparece en tu cámara.</li> 
                    <li>Busca Google Lens y actívalo</li>
                    <li>Ahora ya podrás ver el ícono</li>
                </ol>

                <p>¿Aún no funciona?</p>
                <ol>
                    <li>Escribe el enlace que aparece debajo de tu código QR directamente en tu navegador</li>
                </ol>

            </QRinst>
            <style jsx global>{`
                @media print {
                    header, footer, #fb-root {
                        display: none;
                    }
                }
            `}</style>
        </OnlyIfLogin>
    )
}
export default withApollo({ ssr: true })(QrPage);

const Buttons = styled.div`
    margin-top: 2rem;
    display: flex;
    justify-content: space-evenly;
    max-width: 320px;
    margin: auto;

    @media print {
        display: none;
    }
`;

const QRinst = styled.div`
    margin-top: 2rem;
    padding-right: 1rem;
    @media print {
        display: none;
    }

    ol {
        font-size: 0.8rem;
        text-align: left;
        max-width: 400px;
        margin: auto;
    }

    li {
        img {
            width: 20px;
        }
    }
`;
