import { withApollo } from '../../utils/apollo'
import OnlyIfLogin from '../../components/common/OnlyIfLogin'

import ShowMyMenus from '../../components/store/ShowMyMenus';

function SeeMenus() {
    return (
        <OnlyIfLogin>
            <ShowMyMenus/>
        </OnlyIfLogin>
    )
}

export default withApollo()(SeeMenus)
