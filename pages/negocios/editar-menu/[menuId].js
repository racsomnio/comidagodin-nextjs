import { withApollo } from '../../../utils/apollo'
import OnlyIfLogin from '../../../components/common/OnlyIfLogin'
import GetMenu from '../../../components/menu/GetMenu'

const EditMenu = () => {
    return (
        <OnlyIfLogin>
            <GetMenu />
        </OnlyIfLogin>
    )
}

export default withApollo()(EditMenu)
