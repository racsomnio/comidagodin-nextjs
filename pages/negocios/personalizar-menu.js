import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'
import OnlyIfLogin from '../../components/common/OnlyIfLogin'
import { withApollo } from '../../utils/apollo'
import { GET_CURRENT_USER} from '../../queries'
import Loading from '../../components/common/Loading'
import Error from '../../components/common/Error'
import PickColor from '../../components/menu/PickColor'

const CustomMenuPage = () => {
    const { data, loading, error } = useQuery(GET_CURRENT_USER)

    if(loading) return <Loading />
    if(error) return <Error error={error} />

    const { slug, rights } = data.getCurrentUser;
    const has_rights = !rights.includes('FREE');

    return (
        <OnlyIfLogin>
            <h1>Personaliza Tu Menú</h1>
            {
                has_rights 
                ? 
                    <PickColor slug={slug} />
                : 
                    <div>
                        <p>
                            Contrata uno de nuestros planes para que puedas personalizar tu menú.
                        </p>
                        <div>
                            <Link href="/negocios/ver-planes">
                                <a className="button secondary">Ver Planes</a>
                            </Link>
                        </div>
                    </div>
                    
            }
            
        </OnlyIfLogin>
    )
}

export default withApollo({ ssr: true })(CustomMenuPage)
