import { useRouter } from 'next/router'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'
import { Icon } from '../../styles/common';
import { GET_STORE } from '../../queries';
import Loading from '../../components/common/Loading';
import Error from '../../components/common/Error';
import ShowLocation from '../../components/common/ShowLocation';
import { withApollo } from '../../utils/apollo'
import { countryToFlag } from '../../utils'

function Store() {
    const router = useRouter();
    const { storeId } = router.query;

    const { loading, error, data } = useQuery(GET_STORE, {
        variables: { _id: storeId }
    })

    if(loading) return <Loading/>;
    if(error) return <Error error={error}/>
    
    return (
        <div className="section">
            <H1>{data.getStore.name} <span>( {data.getStore.typeOfBusiness})</span></H1> 


            <Banner style={{ backgroundImage: `url(${data.getStore.cover_img.length ? data.getStore.cover_img[0].secure_url : '/no-image.png'})` }}>
                <CardContent>
                    <div className="card-logo"
                        style={{ 
                            background: `url(${data.getStore.profile_img.length ? data.getStore.profile_img[0].secure_url : '/no-image.png'})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center'
                        }}
                    />
                </CardContent>
            </Banner>

            <ShowLocation location={data.getStore.location}>
                {data.getStore.location.address}
            </ShowLocation>

            <GridContainer>
                {   
                    data.getStore.speciality.length ? 
                    <div>
                        Nos especializamos en: <MainTag>{data.getStore.speciality[0]}</MainTag>
                    </div>
                    : null
                }

                <div className="col">
                    <Tel>
                        <a href={`tel:+52${data.getStore.phone[0].number}`}>
                            {data.getStore.phone[0].number}
                        </a>
                    </Tel>
                    
                    {
                        data.getStore.phone[0].whatsapp
                        ? (
                            <SendWhatsapp>
                                <a 
                                    href={`https://wa.me/52${data.getStore.phone[0].number}?text=Hola%2C%20v%C3%AD%20tu%20men%C3%BA%20en%20Comida%20God%C3%ADn%2C%20`}
                                >
                                    <Icon><img src='/icons/whatsapp.svg'/></Icon>
                                </a>
                            </SendWhatsapp>
                        )
                        : null
                    }
                </div>
            </GridContainer>

{/*
            <GridCols>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/table.svg'}/></Icon>
                    <span>10<br/><small>Max. p/Mesa</small></span>
                </div>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/order.svg'}/></Icon>
                    <span>50<br/><small>Max. Órdenes p/Llevar</small></span>
                </div>
            </GridCols>

            <Divider/>

            <GridCols>
                <div className="col">
                    <span>Lunes a Viernes</span>
                    <span>
                        <Icon><img src={process.env.PUBLIC_URL + '/icons/time.svg'}/></Icon>
                        <span>6am - 9pm</span>
                    </span>
                    <span>
                        <Icon><img src={process.env.PUBLIC_URL + '/icons/happyhour.svg'}/></Icon>
                        <span>5pm - 7pm</span>
                    </span>
                </div>
                <div className="col">
                    <span>Sábados</span>
                    <span>
                        <Icon><img src={process.env.PUBLIC_URL + '/icons/time.svg'}/></Icon>
                        <span>6am - 5pm</span>
                    </span>
                </div>
            </GridCols>
            
            <Divider/>
*/}
            
{/*
            <GridCols>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/facebook.svg'}/></Icon>
                </div>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/instagram.svg'}/></Icon>
                </div>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/twitter.svg'}/></Icon>
                </div>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/pinterest.svg'}/></Icon>
                </div>
                <div>
                    <Icon><img src={process.env.PUBLIC_URL + '/icons/website.svg'}/></Icon>
                </div>
            </GridCols>

<Divider/> */}
            <GridTags>
                { data.getStore.food_type.type && 
                    <Tag>
                        <span style={{ fontSize: '1.5rem', verticalAlign: 'middle' }}>{countryToFlag(data.getStore.food_type.code)}</span>
                        <span style={{ verticalAlign: 'middle', marginLeft: '0.5rem' }}>Comida {data.getStore.food_type.type}</span>
                    </Tag>
                }
                
                {
                    data.getStore.delivery && (
                        <Tag>
                            <Icon><img src='/icons/delivery.svg'/></Icon>
                            <span>Entrega a Domicilio</span>
                        </Tag>
                    )
                }

                {
                    data.getStore.takeaway && (
                        <Tag>
                            <Icon><img src='/icons/takeaway.svg'/></Icon>
                            <span>Para Llevar</span>
                        </Tag>
                    )
                }
                
                {
                    data.getStore.cash && (
                        <Tag>
                            <Icon><img src='/icons/cash.svg'/></Icon>
                            <span>Sólo Efectivo</span>
                        </Tag>
                    )
                }
            
                {
                    data.getStore.card && (
                        <Tag>
                            <Icon><img src='/icons/card.svg'/></Icon>
                            <span>Aceptamos Tarjetas</span>
                        </Tag>
                    )
                }
                {
                    data.getStore.open_tv && (
                        <Tag>
                            <Icon><img src='/icons/tv.svg'/></Icon>
                            <span>TV abierta</span>
                        </Tag>
                    )
                }

                {
                    data.getStore.cable && (
                        <Tag>
                            <Icon><img src='/icons/cable.svg'/></Icon>
                            <span>Cable</span>
                        </Tag>
                    )
                }
                
                {
                    data.getStore.ppv && (
                        <Tag>
                            <Icon><img src='/icons/ppv.svg'/></Icon>
                            <span>PPV</span>
                        </Tag>
                    )
                }
                {
                    data.getStore.speak_english && (
                        <Tag>
                            <Icon><img src='/icons/speaking.svg'/></Icon>
                            <span>We speak English</span>
                        </Tag>
                    )
                }
                
                {
                    data.getStore.english_menu && (
                        <Tag>
                            <Icon><img src='/icons/menu.svg'/></Icon>
                            <span>English Menu</span>
                        </Tag>
                    )
                }
                {
                    data.getStore.vegan_option && (
                        <Tag>
                            <Icon><img src='/icons/vegan.svg'/></Icon>
                            <span>Opción Vegana</span>
                        </Tag>
                    )
                }
                
                {
                    data.getStore.vegetarian_option && (
                        <Tag>
                            <Icon><img src='/icons/vegetarian.svg'/></Icon>
                            <span>Opción Vegetariana</span>
                        </Tag>
                    )
                }

                {
                    data.getStore.no_gluten_option && (
                        <Tag>
                            <Icon><img src='/icons/gluten-free.svg'/></Icon>
                            <span>Opción Sin Gluten</span>
                        </Tag>
                    )
                }
                
                {
                    data.getStore.no_shellfish_option && (
                        <Tag>
                            <Icon><img src='/icons/shrimp.svg'/></Icon>
                            <span>Opción sin Mariscos</span>
                        </Tag>
                    )
                }
            </GridTags>

        </div>
    )
}

export default withApollo({ ssr: true })(Store)

const Img = styled.div`
    img{
        border-radius: 10px;
        margin-top: 2rem;

        @media all and (min-width: 768px){
            min-width: 500px;
            margin-top: 0;
        }
        
    }
`;

const CardContent = styled.div`
    position: absolute;
    top: 100%;
    text-align: right;
    text-align: center;
    width: 100%;

    .card-logo {
        width: 150px;
        height: 150px;
        border: 5px solid #f2f2f2;
        border-radius: 50%;
        margin: auto;
        margin-top: -80px;
    }
`;

const Banner = styled.div`
  background-size: auto;
  background-repeat: no-repeat;
  background-position: bottom center;
  background-attachment: fixed;
  height: 60vh;
  max-width: 960px;
  margin: auto;
  position: relative;

  @media all and (min-width: 500px) {
    background-size: contain;
  }

  @media all and (min-width: 900px) {
    background-position: calc(50% + 37px);
  }
  

  @supports (-webkit-overflow-scrolling: touch) {
    background-attachment: scroll;
  }

  & + * {
    margin: 85px 0 0;
  }
`;

const H1 = styled.h1`
    text-transform: capitalize;
    font-family: 'Baloo 2',cursive;

    span {
        display: block;
        font-size: 0.8rem;
    }
`;

const MainTag = styled.span`
    background: #fbda6d;
    border-radius: 10px 0;
    padding: 0.5rem;
    vertical-align: middle;
    font-size: 0.9rem;
    font-weight: bold;
`;

const Tag = styled.span`
    background: #f5f1f1;
    border-radius: 10px 0;
    padding: 0.5rem;
    vertical-align: middle;
    font-size: 0.9rem;
    font-weight: bold;
    margin: 1rem 0.5rem 0;
`;

const Tel= styled.span`
    background: #000;
    color: #fff;
    font-weight: bold;
    border-radius: 10px 0;
    padding:0.5rem;
    font-size: 0.9rem;
`;

const SendWhatsapp = styled.span`
    padding: 0.2rem;
    border-radius: 10px;
    position: relative;
    font-size: 0.8rem;
    margin: 1rem auto 0;
    padding: 0.5rem;
    border-radius: 50%;
`;

const GridTags = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    max-width: 800px;
    margin: auto;
`;

const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(320px, 1fr));
    margin: auto;
    max-width: 800px;
    grid-auto-rows: 120px;
    align-items: center;
`;
