import { useState } from 'react'
import styled from 'styled-components'
import Link from 'next/link'
import OnlyIfLogin from '../../components/common/OnlyIfLogin'

function AddMenuPage() {
    const [ toolkit, setToolkit ] = useState(false);

    function copyToClipboard (str) {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        setToolkit(true);
        timer();
    }; 

    function timer() {
        setTimeout(() => {
            setToolkit(false);
        }, 1000)
    }

    return (
        <>
            <h1>Nosotros Lo Digitalizamos</h1>
            <Grid>
                <PlanGold>
                    <strong>Envíanos Un Email</strong><br/>
                    <p>Tu menú debe ser envíado desde la misma dirección con la que te registraste aquí</p>
                    <p>Puedes adjuntar fotos si lo deseas.</p>
                    <p>
                        Puedes en enviarnos un correo a:<br/>
                        <strong>hola@comidagodin.com</strong>
                    </p>
                        <div 
                            className={`button ${toolkit && 'show'}`}
                            onClick={ () => copyToClipboard("hola@comidagodin.com")}
                        >
                            Copiar Email
                        </div>
                    <p>O dar clic en el siguiente botón</p>
                    <a className="button secondary" href="mailto:hola@comidagodin.com?subject=Digitaliza%20mi%20menú" target="_blank">Enviar</a>
                    <br/>
                </PlanGold>
            </Grid>
        </>
    )
}

export default AddMenuPage

const PlanGold = styled.div`
    max-width: 320px;
    background: #fff;
    border: 1px solid #e6e7e8;
    border-radius: 10px;
    margin-top: 1rem;
    margin-bottom: 2rem;

    & > strong {
        display: block;
        padding: 1rem 0;
        background: #fbdb6d;
        border-radius: 10px 10px 0 0;
    }

    p {
        text-align: left;
        margin-top: 0;
        margin-bottom: 2rem;
        padding: 0 1rem;
        font-size: 0.9rem;
    }

    .button {
        display: inline-block;
        margin: 0 auto 2rem;
        position: relative;

        &.show {
            &:before {
                content:"copiado";
                background: rgba(0,0,0,0.5);
                border-radius: 20px;
                color: #fff;
                position: absolute;
                bottom: 100%;
                left: 50%;
                padding: 0 0.5rem;
                transform: translateX(-50%);
                opacity: 1;
            }
        }
    }
`;

const Grid = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    padding: 1rem;
`;
