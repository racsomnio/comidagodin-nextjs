import { useState } from 'react'
import Router from 'next/router'
import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'
import { withApollo } from "../../utils/apollo";
import styled from 'styled-components'
import Upgrade from '../../components/common/Upgrade'
import { GET_CURRENT_USER } from '../../queries'
import Loading from '../../components/common/Loading'
import ModalLoginRedirect from '../../components/common/ModalLoginRedirect'

const SeePlansPage = () => {
    const [modalOpen, toggleModal] = useState(false);
    const { data, loading } = useQuery(GET_CURRENT_USER);
    if(loading) return <Loading />

    function closeModal() {
        toggleModal(false)
    }

    function handleClickFreePlan() {
        if(data.getCurrentUser){
            Router.push('/negocios/mi-menu')
        }else{
            toggleModal(true)
        }
    }

    return (
        <>
            <h1>Planes</h1>
            <Wrapper>
                
                <Plan>
                    <div>
                        <PlanHeader>GRATUITO</PlanHeader>
                        <div>
                            <span className="price">$0</span>
                            <span className="currency">pesos</span>
                        </div>

                        <Ul>
                            <li>Menú digital</li>
                            <li>Muestra tu logotipo</li>
                            <li>Código QR para acceder al menú</li>
                            <li>Opción para agregar múltiples precios por comida</li>
                            <li>3 foto en tu menú</li>
                            {/* <li>A veces muestra publicidad</li> */}
                        </Ul>
                    </div>
                    
                    <div>
                        <p>
                            <a target="_blank" href="/burros-anzuelo-del-norte" className="button secondary"><strong>DEMO</strong></a>
                        </p>
                        <ModalLoginRedirect isOpen={modalOpen} closeModal={closeModal} >
                            <button role="link" onClick={handleClickFreePlan}>Continuar</button>
                        </ModalLoginRedirect>
                    </div>
                </Plan>
                
                <Plan>
                    <div>
                        <PlanHeader>LIGERO</PlanHeader>
                        <div>
                            <span className="price">$35</span>
                            <span className="currency">pesos</span>
                            <div className="interval">Pago mensual</div>
                        </div>
                        <Ul>
                            <li>Menú digital</li>
                            <li>Código QR para acceder al menú</li>
                            <li>Hasta 8 fotos para tu menú</li>
                            <li>Opción para agregar múltiples precios por comida</li>
                            <li>Opción para agregar etiquetas como: Picoso, Vegetariano, Popular</li>
                            <li>Personaliza tu menú, agrega los colores que se ajusten a tu gusto.</li>
                            {/* <li>Libre de publicidad</li> */}
                        </Ul>   
                        <p>
                            <a target="_blank" href="/mocchi" className="button secondary"><strong>DEMO</strong></a>
                        </p>
                    </div>             
                    <Upgrade plan="PLAN_LITE">
                        <button role="link">
                            Continuar
                        </button>
                    </Upgrade>
                </Plan>
                
                {/* <Upgrade plan="PLAN_PREMIUM">
                    <Plan>
                        <div>
                            <PlanHeader>ALTA COCINA</PlanHeader>
                            <div>
                                <span className="price">$80</span>
                                <span className="currency">pesos</span>
                                <div className="interval">Pago mensual</div>
                            </div>
                            <Ul>
                                <li>Menú digital</li>
                                <li>Muestra tu logotipo</li> */}
                                {/* <li>Opción para quitar el pie de Comida Godín</li> */}
                                {/* <li>Código QR para acceder al menú</li>
                                <li>Hasta 10 fotos para tu menú</li>
                                <li>Opción para agregar más de un precio por comida</li>
                                <li>Opción para agregar etiquetas como: Picoso, Vegetariano, Popular</li> */}
                                {/* <li>Opción para crear código QR para tu WIFI</li> */}
                                {/* <li>Opción para añadir tu redes sociales</li> */}
                                {/* <li>Incluye buscador dentro de tu menú</li> */}
                                {/* <li>Opción para crear una lista de espera</li> */}
                                {/* <li>Opción para mostrar menú en otro idioma(s)</li>
                                <li>Traducimos tu menú en Inglés y/o Japonés</li>
                                <li>Personaliza tu menú</li>
                                <li>Libre de publicidad</li>
                                <li>No muestra logo de Comida Godín</li>
                            </Ul>
                        </div> */}
                    
                        {/* <button role="link">
                            Continuar
                        </button>
                    </Plan>
                </Upgrade> */}
                <div>
                    <Plan>
                        <div>
                            <PlanHeader>A LA MEDIDA</PlanHeader>

                            <Ul>
                                <li>¿Necesitas más fotos para tu menú?</li>
                                <li>¿Una página web?</li>
                                <li>¿Quieres usar nuestro servicios pero dentro de tu sitio web?</li>
                                <li>¿Quieres tu menú libre de publicidad y con otro dominio?</li>
                            </Ul>
                            
                            <p style={{ fontSize: '0.9rem', marginBottom: '2rem' }}>Envíanos un correo ó contáctanos por el messenger chat en esta página. Tenemos soluciones para tu negocio.</p>
                            
                            <a className="button secondary" href="mailto:hola@comidagodin.com" target="_blank" rel="noopener">
                                Contáctanos
                            </a>
                        </div>
                    </Plan>
                </div>
            </Wrapper>
        </>
    )
}

const Wrapper= styled.div`
    display: grid;
    padding: 1rem;
    max-width: 980px;
    margin: auto;
    grid-row-gap: 2rem;
    grid-column-gap: 1rem;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
`;

const Plan= styled.div`

    background: #fff;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    padding: 1rem;
    display: flex; 
    flex-direction: column;
    justify-content: space-between;
    transition: all 0.5s;

    &:hover {
        /* border: 1px solid #7a1a7b; */
        box-shadow: 0 0 1px 10px #7a1a7b;
    }

    .price {
        font-size: 3rem;
        margin-right: 0.5rem;
        vertical-align: middle;
    }

    .currency {
        font-size: 0.8rem;
        vertical-align: middle;
        line-height: 3rem;
    }

    .interval {
        font-size: 0.8rem;
    }
`;

const PlanHeader= styled.div`
    text-align: center;
    margin-bottom: 1rem;
`;

const Ul = styled.ul`
    text-align: left;
    margin-top: 2rem;
    li {
        list-style: none;
        padding-bottom: 1rem;
        font-size: 0.9rem;
    }
`;

export default withApollo({ ssr: true })(SeePlansPage);
