import React from 'react'

function Negocios() {
    return (
        <div style={{ maxWidth: '500px', margin: 'auto', padding: '1rem' }}>
            <h1>¿Cómo funciona?</h1>
            <p>
                Crea una copia digital de tu menú en 3 pasos:
            </p>
            <span
                style={{ background: '#7a1a7b', borderRadius: '20px', padding: '1rem', color: '#fff', marginTop: '1rem', display:'inline-block' }}
            >1</span>
            <p>Registra tu Negocio</p>

            <span
                style={{ background: '#7a1a7b', borderRadius: '20px', padding: '1rem', color: '#fff', marginTop: '1rem', display:'inline-block' }}
            >2</span>
            <p>Dinos más acerca de tu establecimiento</p>

            <span
                style={{ background: '#7a1a7b', borderRadius: '20px', padding: '1rem', color: '#fff', marginTop: '1rem', display:'inline-block' }}
            >3</span>
            <p>Sube tu menú</p>

            <strong style={{ marginTop: '2rem' }}>Una vez que hayas subido tu menú, nuestros algoritmos harán que seas visible para nuestra familia Godín cerca de ti.</strong>

            <p>También puedes compartir tu enlace por tus redes sociales, ó descargar un código QR para que tus clientes tengas acceso a él sin necesidad de escribirlo.</p>
        </div>
    )
}

export default Negocios
