import { useRouter } from 'next/router'

function RevisaEmail() {
    const router = useRouter();
    const { email } = router.query;

    return (
        <div>
            <p>Hemos enviado un correo electrónico a:</p>
            <strong>{email}</strong>
            <p>
                <small>Sigue las intrucciones para que puedas actualizar tu contraseña</small>
            </p>
        </div>
    )
}

export default RevisaEmail
