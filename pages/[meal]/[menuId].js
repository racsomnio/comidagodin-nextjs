import { useRouter } from 'next/router'
import { withApollo } from "../../utils/apollo"
import FullMenu from '../../components/menu/FullMenu'


function GetMenu() {
    const router = useRouter()
    const { meal, menuId } = router.query;
    return (
        <>
            <FullMenu meal={meal} menuId={menuId} />
        </>
    )
}

export default withApollo({ ssr: true })(GetMenu);
