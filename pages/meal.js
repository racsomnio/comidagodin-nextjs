import { useRouter } from 'next/router'
import { daysLong, swap_it } from '../utils';
import { withApollo } from "../utils/apollo";
import MenuList from '../components/menu/MenuList'
import { H1 } from '../styles/common'
import dynamic from 'next/dynamic'
const AskGeolocation = dynamic(() => import('../components/common/AskGeolocation'), { ssr: false })

const GetMenus = () => {
    const router = useRouter()
    const { meal } = router.query;
    const today = daysLong[new Date().getDay()];

    if(swap_it[meal] === undefined ) return <div style={{ textAlign: 'center' }}>
            Parece que te has perdido en los pasillos
        </div>

    return (
        <>
            <H1 margin={'0 0 1rem'}>
                {meal}
                <span>Para hoy {today}</span>
            </H1>
            <AskGeolocation>
                <MenuList meal={meal}/>
            </AskGeolocation>
        </>
    )
}

export default withApollo({ ssr: true })(GetMenus);
