import { useRouter } from 'next/router'
import { withApollo } from "../../utils/apollo";
import GetStoresByGeo from '../../components/store/GetStoresByGeo'
import dynamic from 'next/dynamic'
const AskGeolocation = dynamic(() => import('../../components/common/AskGeolocation'), { ssr: false })

const GetMenus = () => {
    const router = useRouter()

    return (
        <>
            <AskGeolocation>
                <GetStoresByGeo />
            </AskGeolocation>
        </>
    )
}

export default withApollo({ ssr: true })(GetMenus);
