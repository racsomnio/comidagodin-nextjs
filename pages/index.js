import {useEffect, useState} from 'react'
import Router from 'next/router'
import Link from 'next/link'
import Head from 'next/head'
import styled from 'styled-components'
import dynamic from 'next/dynamic'
import GodinSection from '../components/home/GodinSection'
import StoreSection from '../components/home/StoreSection'
import Video from '../components/home/BgVideo';

const AskGeolocation = dynamic(() => import('../components/common/AskGeolocation'), { ssr: false })

export default function Home() {
  const [coords, setCoords] = React.useState('');
  const [width, setWidth] = useState(false);

  function updateMedia() {
    setWidth(window.innerWidth >= 768);
  };

  useEffect(() => {
    updateMedia();
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  });

  function updateCoords(coords){
    setCoords(coords)
  }

  return (
    <>
      <Head>
        {/* <meta name="description" content="Satisface tus antojos dentro del rango de tu habitat laboral sin necesidad de pagar de más, sin tarjeta bancaria y todo desde tu teléfono. Ya sea comida, desayuno, cena, café, antojitos, postres o tragos." /> */}
        <meta name="description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta itemProp="name" content="Comida Godín" />
        <meta itemProp="description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta itemProp="image" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta property="og:url" content="https://comidagodin.com" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Comida Godín" />
        <meta property="og:description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta property="og:image:secure_url" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="467" />
        <meta property="og:locale" content="es_MX" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Comida Godín" />
        <meta name="twitter:description" content="En Comida Godín puedes crear tu menú digital GRATIS y recibir pedidos por WhatsApp de una manera mucho mas fácil, sencilla y rápida." />
        <meta name="twitter:image" content="https://comidagodin.com/home/comidagodin.jpg" />
        <meta name="keywords" content="comida godin, menu digital, pedidos por whatsapp"/>
      </Head>

      <Banner>
        <img src="/home/pizza.png" alt="pedidos por whatsapp"/>
        <div>
          <h1>
            ¡Ahórrate una lana!
          </h1>
          <h2><strong>Pide tu comida sin cargos extras.</strong><br/>Mira quien acepta pedidos por WhatsApp cerca de ti.</h2>
          <Link href="/alrededor">
            <a className="button secondary">Buscar Qué Comer</a>
          </Link>
        </div>
      </Banner>

      <Block>
        <StoreSection/>
        
        {/* <GodinSection>
          <p>
              <small>Pero para saber que hay cerca de ti necesitamos saber donde te encuentras.</small>
          </p>
          <AskGeolocation coords={coords} updateCoords={updateCoords} />
        </GodinSection> */}
      </Block>
    </>
  )
}

const Banner = styled.div`
  background: #7a1a7b;
  margin-top: -150px;
  position: relative;
  padding-top: 110px;
  padding-bottom: 3rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media all and (min-width: 980px) {
    flex-direction: row-reverse;
    justify-content: space-evenly;
  }

  img {
    max-width: 400px;
    width: 90%;
  }

  h1 {
    z-index: 0;
    color: #fff;
    font-size: 2rem;
    margin-top: 1rem;
    font-family: "Baloo 2",cursive;
    padding: 0 1rem;
    max-width: 500px;
    margin-bottom: 0;
  }

  h2 {
    color: #fff;
    font-size: 1.2rem;
    font-weight: 300;
    padding: 0 1rem;
    max-width: 500px;
    margin-left: auto;
    margin-right: auto;
    line-height: 1.6rem;
  }

  .button {
    font-size: 1.3rem;
    margin-top: 0.8rem;
  }
`;

const YellowBg = styled.span`
  background: rgb(251, 219, 109);
  border-radius: 3rem;
  padding: 0 1rem;
  line-height: 1.1;
  box-decoration-break: clone;
  font-family: "Baloo 2",cursive;
  font-size: 1.4rem;
`;

const Block = styled.div`
  display: flex;
  flex-direction: column;
`;
