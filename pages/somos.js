import styled from 'styled-components'

const Somos = () => {
    return (
        <SomosStyles>
            <h1>Somos</h1>
            <p>
            Somos un grupo de emprendedores Godín con oficinas en Nueva York y CDMX con amplia trayectoria en el mundo digital, marketing, diseño web, programación y también contamos con gerentes de restaurantes dentro de nuestro equipo godín. 
            </p>
            <p>
                Como buenos godínez buscamos la forma gratuita de pedir y saber donde hay lugares para comer, conocer el menú del día, echar la chela,  degustar las pruebas de pastelitos, y botanear acontecimientos de la semana, así como hacer pedidos a la proximidad de la oficina de forma rápida con apoyo de la tecnología.
            </p>
            <p>Diseñamos soluciones tecnológicas, para fortalecer a los pequeños y medianos negocios de comida para hacerlos resilientes y competitivos ante los cambios imprevistos y puedan adaptarse a una era digital que es mayormente gratuita. </p>
            <img src="/home/somos.jpg" alt="somos"/>
            
        </SomosStyles>
    )
}
export default Somos

const SomosStyles = styled.div`
    max-width: 500px;
    margin: auto;
    padding: 1rem;

    p {
        text-align: justify;
    }

    h3 {
        text-align: left;
    }

    img {
        width: 150px;
        border-radius: 30px;
    }
`;
