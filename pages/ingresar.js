import {useState, useEffect} from 'react'
import Router, { useRouter } from 'next/router'
import Link from 'next/link'
import { withApollo } from "../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import styled from 'styled-components'
import {SINGNIN_USER, GET_CURRENT_USER} from '../queries';
import PasswordEye from '../components/form/PasswordEye'
import Error from '../components/common/Error'

const initialState = {
    email: "",
    password: "",
}

const Ingresar = () => {
    const router = useRouter()
    const { redirect } = router.query;

    const [formValues, setFormValues] = useState(initialState)

    function handleChange(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        signinUser().then(async({data}) => {
            clearState();
            Router.push(`${redirect ? redirect : data.signinUser.__typename == 'Store'? '/negocios/perfil' : '/'}`); 
        });
    }

    function clearState() {
        setFormValues({...initialState});
    }
    
    function validateForm() {
        const {email, password} = formValues;
        const isInvalid = !email || !password;
        return isInvalid;
    }

    const [signinUser, {error, loading}] = useMutation(SINGNIN_USER, {
        variables: formValues,
        refetchQueries: [{query: GET_CURRENT_USER}],
        // awaitRefetchQueries: true,
    });

    return (
        <SigninWrap>
            <h1>Su gafete por favor</h1>

            <Form onSubmit={handleSubmit}>
                <FieldWrap>
                    <input type="email" name="email" placeholder="Email" onChange={handleChange} value={formValues.email} />
                </FieldWrap>

                <FieldWrap>
                    <PasswordEye 
                        name="password" 
                        placeholder="Contraseña" 
                        onChange={handleChange} 
                        value={formValues.password} 
                    />
                </FieldWrap>
                
                <FieldWrap>
                    <button 
                        className="ui button" 
                        type="submit"
                        disabled={loading || validateForm()}
                    >Enviar</button>
                    { error && <Error error={error}/> }
                </FieldWrap>
            </Form>

            <Link href="olvide-contrasena">
                <a className="underlined">
                    <small>Olvidé mi Contraseña</small>
                    </a>
            </Link>
        </SigninWrap>
    )
}

const FieldWrap = styled.div`
    margin-bottom: 1.5rem;
`;

const SigninWrap = styled.div`
    padding: 0 1rem;
`;

const Form = styled.form`
    background: #fff;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    padding: 2rem 1rem 1rem;
    max-width: 400px;
    margin: auto auto 2rem;
`;

export default withApollo({ ssr: true })(Ingresar);
