import { useEffect } from 'react'
import Router, { useRouter } from 'next/router'
import Head from 'next/head'
import { useTransition, animated } from 'react-spring'
import '../styles/global.scss'
import Layout from '../components/layout'
import * as gtag from '../utils/gtag'
import LoadingPage from '../components/common/LoadingPage'
// import MessengerCustomerChat from 'react-messenger-customer-chat';

Router.onRouteChangeStart = () => {
    document.body.classList.add('loading-page');
};
Router.onRouteChangeComplete = () => {
    document.body.classList.remove('loading-page');
};
Router.onRouteChangeError = () => {
    document.body.classList.remove('loading-page');
};

export default function App({ Component, pageProps }) {
    const location  = useRouter()
    const transitions = useTransition( location, location => location.asPath, {
        from: { opacity: 0 },
        enter: { opacity: 1 },
        leave: { visibility: 'hidden'}
    })

    useEffect(() => {
        const handleRouteChange = (url) => {
            gtag.pageview(url)
        }
        Router.events.on('routeChangeComplete', handleRouteChange)
        
        return () => {
          Router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [])

    return (
        <Layout location={location.route}>
            <Head>
                <title>Comida Godín</title>
                <link rel="icon" href="/nav-icons/godin-ico.png" />
            </Head>
            {transitions.map(({ item, props, key }) => (
                <animated.div key={key} style={props}>
                    <Component {...pageProps} />
                </animated.div>
            ))}
            <LoadingPage/>
            {/* {
                location.route !== '/[meal]' &&
                <MessengerCustomerChat
                    pageId="110253954012790"
                    appId="3380325045319553"
                />
            } */}
        </Layout>
    )
}