import {useState} from 'react'
import Router from 'next/router'
import { withApollo } from "../utils/apollo";
import { useMutation } from "@apollo/react-hooks"
import styled from 'styled-components'
import { REQUEST_RESET } from '../queries';
import Error from '../components/common/Error'

const initialState = {
    email: "",
}

const Ingresar = () => {
    const [formValues, setFormValues] = useState(initialState)

    function handleChange(e) {
        const {name, value} = e.target;
        setFormValues(prevState =>({
            ...prevState,
            [name]: value
        }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        requestReset().then(async({data}) => {
            clearState();
            Router.push(`/revisa-tu-email?email=${formValues.email}`); 
        });
    }

    function clearState() {
        setFormValues({...initialState});
    }
    
    function validateForm() {
        const {email} = formValues;
        const isInvalid = !email;
        return isInvalid;
    }

    const [requestReset, {error, loading}] = useMutation(REQUEST_RESET, {
        variables: formValues,
    });

    return (
        <>
            <h1>Olvidé Contraseña</h1>

            <form className="ui form" onSubmit={handleSubmit}>
                <FieldWrap>
                    <input type="email" name="email" placeholder="Email" onChange={handleChange} value={formValues.email} />
                </FieldWrap>

                <FieldWrap double>
                    <button 
                        className="ui button" 
                        type="submit"
                        disabled={loading || validateForm()}
                    >Enviar</button>
                    { error && <Error error={error}/> }
                </FieldWrap>
            </form>
        </>
    )
}

const FieldWrap = styled.div`
    margin-bottom: ${ ({double}) => double ? '3rem' : '1.5rem' };
`;

export default withApollo({ ssr: true })(Ingresar);
