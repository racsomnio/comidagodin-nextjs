import React from 'react'

function Privacidad() {
    return (
        <div style={{ maxWidth: '500px', margin: 'auto', padding: '1rem' }}>
            <h1>Aviso de Privacidad</h1>
            <h2>Objetivo</h2>
            <p>
                Comida Godín se compromete a proteger su privacidad porque estamos comprometidos a valorar a las personas. Nuestra Política de privacidad a continuación establece cómo se recopila, utiliza y protege su información personal.
                Esta Política de privacidad describe nuestras políticas y procedimientos sobre la recopilación, conservación, uso y divulgación de su información personal y debe leerse junto con nuestros Términos y condiciones. Al proporcionar su información personal, acepta nuestra recopilación, uso y divulgación de esa información de acuerdo con esta Política de privacidad.
            </p>

            <h2>¿Qué son los datos personales?</h2>
            <p>Cuando se usa en esta Política, "información personal" tiene el significado dado en la Ley de Privacidad. En general, significa cualquier información u opinión que pueda usarse para identificarlo.</p>

            <h2>Datos personales recopilados</h2>
            <p>Datos personales recopilados para los siguientes fines y utilizando los siguientes servicios:
                Google Analytics: cookies; Datos de uso;
                Formulario de contacto: dirección de correo electrónico, nombre de negocio, número de teléfono, tipo de negocio, ubicación del negocio. 
            </p>

            <h2>Accediendo a sus datos personales</h2>
            <p>Puede solicitar acceso a su información personal recopilada por nosotros y solicitar que corrijamos esa información personal. Puede solicitar acceso o corrección comunicándose con nosotros y generalmente le responderemos dentro de los 30 días. Si nos negamos a darle acceso o corregir su información personal, le notificaremos por escrito explicando los motivos.</p>

            <h2>Quejas</h2>
            <p>
                Si cree que se ha violado su privacidad o si tiene una queja sobre cómo hemos manejado su información personal, contáctenos por escrito. Responderemos dentro de un período razonable (generalmente dentro de los 30 días).
                Si no está satisfecho con nuestra respuesta, puede presentar una queja formal ante la Oficina del Consumidor.
            </p>

            <h2>Propietario y controlador de datos</h2>
            <p>Comida Godín</p>
            <p>Correo electrónico: hola@comidagodin.com</p>
        </div>
    )
}

export default Privacidad
