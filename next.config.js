require('dotenv').config()
const webpack = require('webpack')
// NOTE: *** ALL this are public ***
module.exports = {
  env: {
    'NEXT_PUBLIC_STRIPE_PUBLIC_KEY': process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY,
    'ENDPOINT': process.env.ENDPOINT,
    'NEXT_PUBLIC_PER_PAGE': process.env.NEXT_PUBLIC_PER_PAGE
  }
};