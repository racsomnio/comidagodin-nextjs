import Link from 'next/link'
import { Icon } from '../../styles/common'
import styled from 'styled-components'

function StoreSection() {
    return (
        <>
            <Section vertical>
                <h2>Tengo un Negocio de Comida,<br/>¿Cómo funciona?</h2>
                <div className="digital-menu-btns">
                    <Link href="/negocios/registro">
                        <a className="button">
                            Comienza creando una cuenta
                        </a>
                    </Link>
                </div>
                <div className="store-items">
                    <span className="store-item">
                        <Icon size="80px" block><img src="/home/nosotros-digitalizamos-tu-menu.jpg" alt="Digitalizamos tu menú" /></Icon>
                        <h3>Nosotros Digitalizamos tu Menú</h3>
                        <span> Después de registrarte puedes enviarnos tu menú por correo y nosotros hacemos la captura y digitalización.</span>
                    </span>
                    <span className="store-item">
                        <Icon size="60px" block><img src="/home/pedidos-por-whatsapp.jpg" alt="Digitalizamos tu menú" /></Icon>
                        <h3>Listo! Comienza a recibir pedidos por WhatsApp</h3>
                        <span>Nosotros nos encargamos de que sólo clientes cerca de ti te encuentren. Promoción efectiva dentro de tu área.</span>
                    </span>
                </div>
                <div className="digital-menu-btns">
                    <Link href="/negocios/registro">
                        <a className="button secondary">
                            Regístra Tu Negocio
                        </a>
                    </Link>
                </div>

            </Section>

            <Section secondary>
                <h2 className="digital-menu-title">Preguntas Frecuentes</h2>
                <div className="store-wrap">
                    <div className="digital-menu-info">
                        
                        <div>
                            <h3>¿Qué es Comida Godín?</h3>
                            <div>
                                <p>ComidaGodín es una comunidad que te ayuda a incrementar tus pedidos por WhatsApp, donde nuevos clientes te pueden encontrar fácilmente con promoción efectiva y gratuita dentro de tu área.</p>
                            </div>
                        </div>
                    </div>
                    <div className="digital-menu-wrap">
                        <img src="/home/incrementa-tus-pedidos.jpg" alt="incrementa tus pedidos"/>
                    </div>
                </div>

                <div className="store-wrap">
                    <div className="digital-menu-info">
                        
                        <div>
                            <h3>Ahh, ¿son como UberEats o Rappi?</h3>
                            <div>
                                <p>No, tenemos el mismo concepto, pero nosotros no manejamos transferencias bancarias.</p>
                                <p>Si tu negocio ya esta corriendo con pedidos por teléfono o WhatsApp, aquí es donde nosotros te ayudamos a conseguir más pedidos como esos y ahorrarte cargos extras.</p>
                            </div>
                        </div>
                    </div>
                    <div className="digital-menu-wrap">
                        <img src="/home/nuevos-clientes.jpg" alt="nuevos clientes"/>
                    </div>
                </div>

                <div className="store-wrap">
                    {/* <div className="digital-menu-wrap">
                        <img src="/home/menu-digital-gratis.jpg" alt="menu digital"/>
                    </div> */}
                    <div className="digital-menu-info">
                        <div>
                            <h3>Pero yo ya tengo WhatsApp Business con mi catálogo, ¿Porqué necesitaría mi menú en Comida Godín?</h3>
                            <div>
                                <p>La respuesta corta, efectividad, como buenos godínez la efectivdad laboral es nuestra prioridad.</p>
                                <p>En detalle:</p>
                                <ul>
                                    <li>En Comida Godín recibes en un sólo mensaje la orden completa de tu cliente con nombre y dirección de entrega.</li>
                                    <li>Tu pedido es fácil de compartir con tus repartidores.</li>
                                    <li>WhatsApp Business no te deja poner más de un precio en un solo producto (ej: chico, mediano, grande)</li>
                                    <li>En comida godín tu catálogo es divido en secciones para que tus clienten encuentren facilmente lo que buscan.</li>
                                    <li>También puedes etiquetar tus productos como Popular, Vegetariano, Saludable o Picoso, para dar mas información a tus clientes.</li>
                                    <li>Te ahorramos el tiempo que inviertes en tus redes sociales para promoverte, nosotros nos encargamos de que te encuentren.</li>
                                    <li>
                                        Mira cómo lo hace nuestro primer socio en Comida Godín:<br/>
                                        <Link href="/el-habitat">
                                            <a className="button">
                                                El Hábitat
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </Section>

            <Section vertical>
                <h2>Contáctanos</h2>
                <div className="store-wrap">
                    <p>
                        Envíanos un correo con dudas o si te atoraste en algo. Estamos seguros que juntos lo resolveremos.
                    </p>
                </div>
                <div className="btn-wrap">
                    <a href="mailto:hola@comidagodin.com" className="button" target="_blank" rel="noopener">
                        Contáctanos
                    </a>
                </div>
            </Section>
        </>
    )
}

export default StoreSection

const Section = styled.section`
    padding: 1rem 0 3rem;
    background: ${ ({secondary}) => secondary && '#fbdb6d'};

    h2 {
        padding: 0 1rem;
    }

    .store-wrap {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin-top: 2rem;
        flex-direction: ${ ({vertical}) => vertical ? 'column' : 'row'};
        align-items: center;
    }
    .store-items {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;

        .store-item {
            padding: 1rem;
            margin: 1rem;
            max-width: 300px;
        }

        img {
            border-radius: 50px;
        }
    }
    .digital-menu-info {
        flex: 0 1 320px; 
        font-size: 1.1rem;
    }
    .digital-menu-title {
        font-weight: bold;
    }

    ul, ol {
        text-align: left;
        margin-bottom: 1rem;
        padding-right: 1rem;
        text-align: justify;
    }
    li {
        margin-bottom:1rem;
    }
    .digital-menu-wrap {
        padding: 0 1rem 1rem;

        img {
            max-width: 300px;
            border-radius: 10px;
        }
    }
    .digital-menu-btns {
        a {
            margin: 0.5rem;
        }
    }

    .logo-restaurant {
        margin: 0 1rem;

        img {
            border-radius: 10px;
        }
    }

    p {
        max-width: 600px;
        padding: 0 2rem;
        text-align: justify;
        margin-left: auto;
        margin-right: auto;
    }
    .btn-wrap {
        margin-top: 2rem;
    }
`