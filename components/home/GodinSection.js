import Link from 'next/link'
import { Icon } from '../../styles/common'
import styled from 'styled-components'

function GodinSection({ children }) {
    return (
        <Section>
            <div>
                <Icon size="120px"><img src="/home/street-view.svg" alt="cash" /></Icon>
                <h3>Colega godín,</h3>
                <p>Estamos construyendo algo para ti y una manera para ayudarnos entre todos.</p>
                <p>Síguenos en nuestras redes sociales para actualizaciones.</p>
                <div>
                    <span className="social">
                        <a href="https://www.facebook.com/comida.gdn" rel="noopener" target="_blank"><Icon size="30px"><img src="/icons/facebook.svg" alt="comida godín facebook"/></Icon></a>
                    </span>
                    <span className="social">
                        <a href="https://twitter.com/comida_gdn" rel="noopener" target="_blank"><Icon size="30px"><img src="/icons/twitter.svg" alt="comida godín twitter"/></Icon></a>
                    </span>
                </div>
                {/* <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center',}}>
                    <Icon size="50px" style ={{ flex: '0 0 50px'}}><img src="/home/order.svg" alt="cash" /></Icon>
                    <small style={{ whiteSpace: 'normal', textAlign: 'left', paddingLeft: '1rem'}}>
                    <strong>¿No sabes que comer hoy?</strong> 
                    <br/>
                    Satisface tus antojos sin necesidad de pagar de más, sin tarjeta bancaria y todo desde tu teléfono. 
                    </small>
                </div>

                <div style={{ display: 'flex',justifyContent: 'space-between', alignItems: 'center', paddingTop: '2rem'}}>
                    <Icon size="50px" style ={{ flex: '0 0 50px'}}><img src="/home/menu.svg" alt="menu" /></Icon>
                    <small style={{ whiteSpace: 'normal', textAlign: 'left', paddingLeft: '1rem'}}>
                    <strong>Menú del día</strong>
                    <br/>
                    Encuentra los especiales por la mañana ó por la tarde, incluso en fines de semana. 
                    <br/>
                    <Link href="/comidas">
                        <a>#horadelapapa</a>
                    </Link>
                    &nbsp; <Link href="/desayunos"><a>#lamañanera</a></Link>
                    </small>
                </div>

                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', paddingTop: '2rem'}}>
                    <Icon size="50px" style ={{ flex: '0 0 50px'}}><img src="/home/bakery.svg" alt="pastel" /></Icon>
                    <small style={{ whiteSpace: 'normal', textAlign: 'left', paddingLeft: '1rem'}}>
                    <strong>¿Cumpleaños en la oficina?</strong>
                    <br/>
                    Explora tus opciones, tal vez un pastel ó tal vez comida en grupo.
                    <br/>
                    <Link href="/postres"><a>#cumpleañosgodin</a></Link>
                    </small>
                </div>

                <div style={{ display: 'flex',justifyContent: 'space-between', alignItems: 'center', paddingTop: '2rem'}}>
                    <Icon size="50px" style ={{ flex: '0 0 50px'}}><img src="/home/no-pig.svg" alt="piggy"/></Icon>
                    <small style={{ whiteSpace: 'normal', textAlign: 'left', paddingLeft: '1rem'}}>
                    <strong>Vence el mal del puerco</strong>
                    <br/>
                    Recupera tu productividad con el poder de la cafeína.
                    <br/>
                    <Link href="/bebidas"><a>#juntoscontraelmaldelpuerco</a></Link>
                    </small>
                </div>

                <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', paddingTop: '2rem'}}>
                    <Icon size="50px" style ={{ flex: '0 0 50px'}}><img src="/home/soccer.svg" alt="drink" /></Icon>
                    <small style={{ whiteSpace: 'normal', textAlign: 'left', paddingLeft: '1rem'}}>
                    <strong>Escápate para ver el fut</strong>
                    <br/>
                    Busca restaurantes con TV o cable.
                    <br/>
                    <Link href="/tragos"><a>#chelasyfut</a></Link>
                    </small>
                    
                </div> */}

                {/* {children} */}
            </div>
        </Section>
    )
}
export default GodinSection;

const Section = styled.section`
    max-width: 320px;
    padding: 1rem;
    margin: auto;

    .social {
        width: 30px;
        display: inline-block;
        margin: 0 1rem;
    }
`;