import {useState} from 'react'

function BgVideo() {
    const [loading, setLoading] = useState(true)

    const handleMetadata = event => {
        setLoading(false);
    }

    return (
        <video
            autoPlay
            muted
            loop
            onLoadedMetadata={handleMetadata}
            style={{
                position: "absolute",
                width: "100%",
                left: 0,
                top: 0,
                opacity: loading ? 0 : 1,
                transition: "opacity, 2s ease-in-out"
            }}
      >
        <source src="/home/homeVideo.mp4" type="video/mp4" />
      </video>
    )
}

export default BgVideo
