import {useState} from 'react'
import { Checkbox } from '../../styles/common';
import styled from 'styled-components'

const AskUserInfo = ({handleOnChange, user}) => {
    return (
        <AskUserInfoWrapper>
            <div>
                <h3>Forma de Entrega</h3>
                <CheckboxParent>
                    <span>Lo Paso a Recoger</span>
                    <Checkbox className="switch">
                        <input type="radio" name="entrega" value="pickup" onChange={handleOnChange} />
                        <span className="slider"></span>
                    </Checkbox>
                </CheckboxParent>
                <CheckboxParent>
                    <span>Entrega a domicilio</span>
                    <Checkbox className="switch">
                        <input type="radio" name="entrega" value="delivery" onChange={handleOnChange}/>
                        <span className="slider"></span>
                    </Checkbox>
                </CheckboxParent>
            </div>
            <h3>A nombre de:</h3>
            <p>
                <input type="text" name="nombre" value={user.nombre} onChange={handleOnChange} />
            </p>
            {
                user.entrega === 'delivery' &&
                <div>
                    <h3>Dirección</h3>
                    <p>
                        <input type="text" name="calle" placeholder="Calle" value={user.calle} onChange={handleOnChange} />
                    </p>
                    <p>
                        <input type="text" name="num" placeholder="Número" value={user.num} onChange={handleOnChange} />
                    </p>
                    <p>
                        <input type="text" name="apt" placeholder="apt" value={user.apt} onChange={handleOnChange} />
                    </p>
                    <p>
                        <input type="text" name="cp" placeholder="C.P." value={user.cp} onChange={handleOnChange} />
                    </p>
                </div>
            }
        </AskUserInfoWrapper>
    )
}

export default AskUserInfo

const CheckboxParent = styled.div`
    text-align: left;
    display: block;
    width: 100%;
    max-width: 260px;
    margin: auto auto 1rem;
    padding-bottom: 1rem;
`;

const AskUserInfoWrapper = styled.div`
    margin-bottom: 5rem;

    input {
        width: 100%;
    }
`;