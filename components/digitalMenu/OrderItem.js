import styled from 'styled-components'
import Modal from '../common/Modal'

const OrderItem = ({ order, showOrderItem, closeOrderItem, addItem, removeItem, itemSelected, handleItemSelected }) => {

    function updateFinalOrder(){
        addItem(itemSelected);
        closeOrderItem();
    }

    function handleAddQty(qty) {
        handleItemSelected({
            ...itemSelected,
            qty
        })
    }

    function handleReqs(e) {
        const { value } = e.target;
        handleItemSelected({
            ...itemSelected,
            special_request: value,
        })
    }

    function handleClickPriceWhenMany(index) {
        handleItemSelected({
            ...itemSelected,
            priceSelected: index
        })
    }
    
    const isInFinalOrder = order.some( item => item.food_name === itemSelected.food_name && item.priceSelected == itemSelected.priceSelected)

    return (
        <Modal
            isOpen={showOrderItem} 
            closeModal={closeOrderItem}
        >
            <ModalHeader>
                {itemSelected.food_name}
            </ModalHeader>

            <div>
                {
                    itemSelected.price 
                    && itemSelected.price.length === 1 
                    && 
                    <Price single>
                        {
                        `$${itemSelected.price[0].price_value * itemSelected.qty} ${itemSelected.price[0].price_description}`
                        }
                    </Price>
                }
                
                {
                    itemSelected.price && itemSelected.price.length > 1 &&
                    <div>
                        <h4>Selecciona uno</h4>
                        <PriceContainer>
                            {
                                itemSelected.price.map( (p, i) => 
                                    <Price 
                                        onClick={() => handleClickPriceWhenMany(i)}
                                        key={p.price_value}
                                        className={`${itemSelected.priceSelected == i ? 'selected' : ''}`}
                                    >
                                        <span>${p.price_value * itemSelected.qty} </span>
                                        <span>{p.price_description}</span>
                                    </Price>
                                )
                            }
                        </PriceContainer>

                    </div>
                }
            </div>
            <h4>¿Cuántos?</h4>
            <p>
            {
                Array.from({length: 6}, (k, index) => 
                    <QtyBtn className={`${itemSelected.qty === index + 1 && 'selected'}`} key={index} onClick={() => handleAddQty(index + 1)}>{index + 1}</QtyBtn>
                )
            }
            </p>
            <Instructions>
                <h4>Instrucciones:</h4>
                <textarea onChange={handleReqs} value={itemSelected.special_request}></textarea>
            </Instructions>
            {
                isInFinalOrder &&
                <p>
                    <button 
                        className="alert"
                        onClick={() => removeItem(itemSelected)}
                    >
                        Remover
                    </button>
                </p>
            }
            <p>
                <WhatsappBtn onClick={updateFinalOrder} >{ !isInFinalOrder ? 'Agregar a mi pedido' : 'Actualizar Pedido'}</WhatsappBtn>
            </p>
        </Modal>
    )
}

export default OrderItem;

const ModalHeader = styled.h2`
    padding-bottom: 1rem;
    border-bottom: 1px solid #bbb;
`;

const Instructions = styled.div`
    margin-top: 3rem;
    margin-bottom: 2rem;

    h4 {
        margin-bottom: 0.5rem;
    }

    textarea {
        border: 1px solid #e6e7e8;
        border-radius: 10px;
        padding: 0.5rem 1rem;
        font-family: inherit;
        font-size: 1rem;
        outline: none;
    }
`;

const WhatsappBtn = styled.button`
    background: #7a1a7b url('/icons/whatsapp.svg') no-repeat;
    background-size: 30px;
    background-position: 1rem center;
    background-repeat: no-repeat;
    padding: 1rem 1rem 1rem 55px;
    line-height: 1;
`;

const PriceContainer = styled.div`
    display: flex;
    justify-content: space-evenly;
`;

const Price = styled.span`
    background: ${({single}) => single ? '#fbdb6d' : '#f0f0f0'};
    border-radius: 30px;
    padding: 0.5rem 1rem;

    &.selected {
        background: #fbdb6d;
    }
`;

const QtyBtn = styled.span`
    background: #f0f0f0;
    color: #999;
    padding: 0.5rem;
    margin: 0 0.5rem;
    border-radius: 10px;
    transition: background 0.5s;
    cursor: pointer;

    &.selected {
        background: #7a1a7b;
        color: #fff;
    }
`;