import styled from 'styled-components';
import Link from 'next/link';
// import { Divider } from './styles/common';

const Footer = () => {
    return (
        <footer>
            <div className="footer-links">
                <div className="footer-col">
                    <strong>Nosotros</strong>
                    <ul>
                        <li>
                            <Link href="/somos">
                                <a>
                                    ¿Quiénes somos?
                                </a>
                            </Link>
                        </li>
                        <li>
                            <a href="mailto:hola@comidagodin.com" target="_blank" rel="noopener">
                                Contáctanos
                            </a>
                        </li>
                    </ul>
                </div>

                <div className="footer-col">
                    <strong>Negocios</strong>
                    <ul>
                        <li>
                            <Link href="/negocios/registro">
                                <a>
                                    Regístrame
                                </a>
                            </Link>
                        </li>
                    </ul>
                </div>

                {/* <div className="footer-col">
                    <strong>Godín</strong>
                    <ul>
                        <li>¿Cómo funciona?</li>
                        <li>Preguntas Frecuentes</li>
                        <li>Regístrame</li>
                    </ul>
                </div> */}
            </div>
            <span>&copy; Comida Godín {new Date().getFullYear()}</span>
            &nbsp; Todos los derechos reservados &bull; 
            <Link href="/terminos-y-condiciones">
                <a>Términos y condiciones</a> 
            </Link>
            &bull; 
            <Link href="/aviso-de-privacidad">
                <a>Aviso de privacidad</a> 
            </Link> 
            &bull; Mapa del sitio &bull;
        </footer>
    )
}

export default Footer
