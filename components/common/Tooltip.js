import React from 'react'
import styled from 'styled-components'

const Tooltip = ({ children, message, error, show, showOnFocus, showOnHover }) => {
    return (
        <Container>
            { children }
            <TooltipWrap 
                showOnFocus={showOnFocus} 
                showOnHover={showOnHover}
                className={ `${show && 'isVisible'} ${error && 'isVisible error'}`}
            >
                { error ? error : message }
            </TooltipWrap>
        </Container>
    )
}

export default Tooltip;

const Container = styled.span`
    position: relative;
    display: block;
`;

const TooltipWrap = styled.span`
    background: black;
    position: absolute;
    color: #fff;
    border-radius: 0 0 10px 10px;
    left: 50%;
    transform: translateX(-50%);
    top: 100%;
    font-size: 0.8rem;
    width: 86%;
    opacity: 0;
    height: 0;
    padding: 0;
    z-index:1;
    transition: all 0.3s;

    &.isVisible, 
    ${ ({showOnHover}) =>  showOnHover ? `input:hover ~ &` : `.null`},
    ${ ({showOnFocus}) =>  showOnFocus && `input:focus ~ &`} {
        padding: 0.8rem;
        height: auto;
        opacity: 1;
    }

    &.error{
        padding: 0.8rem;
        height: auto;
        opacity: 1;
        background: #e00404;
    }
`;
