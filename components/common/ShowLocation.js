import { useState } from 'react';
import Head from 'next/head'
import styled from 'styled-components'
import { Icon } from '../../styles/common';
import { useTransition, animated } from 'react-spring';
import dynamic from 'next/dynamic'
const ShowMap = dynamic(
    () => import('./ShowMap'),
    { ssr: false }
)

const ShowLocation = ({location, children}) => {
    const [ open, toggle ] = useState(false);

    const transitions = useTransition( open, null, {
        from: { opacity: '0' },
        enter: { opacity: '1'},
        leave: { opacity: '0' }
    })

    const handleClick = () => {
        toggle(!open);
    }

    return (
        <>
            <Head>
                <link rel="stylesheet" href="/leaflet.css" />
            </Head>

            <LocationWrap onClick={handleClick}>
                <Icon size={'20px'}><img src="/icons/pin.svg" alt="mapa" /></Icon>
                <span className="underlined" style={{ cursor: 'pointer' }}>{children}</span>
            </LocationWrap>

            {transitions.map(
                ({ item, key, props }) =>
                    item && (
                    <animated.div 
                        key={key} 
                        style={{ 
                            ...props, 
                            position: 'fixed', 
                            top: '0', 
                            right: '0',
                            bottom: '0',
                            left: '0',
                            overflow: 'hidden',
                            zIndex: '100'
                        }
                    }>
                        <ShowMap location={location} close={handleClick}/>
                    </animated.div>
                )
            )}
        </>
    )
}

const LocationWrap = styled.div`
    display: flex;
    align-items: center;

    ${Icon} {
        flex: 0 0 22px;
    }
`;

export default ShowLocation
