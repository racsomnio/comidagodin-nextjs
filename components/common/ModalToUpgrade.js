import { useState } from 'react'
import Modal from './Modal'

const ModalLoginRedirect = ({children, callback, disabled, isPic}) => {
    const [isOpen, toggleOpen] = useState(false);

    function toggleModal() {
        toggleOpen(!isOpen)
    }

    return (
        <div>
            <span onClick={() => disabled && toggleModal()}>
                {children}
            </span>
            
            <Modal isOpen={isOpen} closeModal={toggleModal}>
                <p>Sólo disponible en <strong>Plan Ligero</strong></p>
                {
                    isPic && 
                    <strong>Puedes agregar una foto al final de la categoría. Busca el botón morado que dice <span style={{ background: '#7a1a7b',color: '#fff', borderRadius: '30px', padding: '0.3rem', fontSize: '0.7rem' }}>Foto</span></strong>
                }
                
                <p>Agregálo por sólo $35 pesos/mes </p>

                <div style={{ marginTop: '1.5rem' }}>
                    <button className="secondary" onClick={callback}>
                        Más información
                    </button>
                </div>
            </Modal>
        </div>
    )
}

export default ModalLoginRedirect