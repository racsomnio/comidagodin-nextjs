import styled from 'styled-components'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { STORE_PIC, GET_MY_PICS } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'

function StoredPics({ getImage, isOpen, close, storeId }) {
    const {data: picData, loading: picLoading, error: picError} = useQuery(GET_MY_PICS, {
        variables: {_id : storeId}
    });
    const [storePic, {loading}] = useMutation(STORE_PIC,{
        variables: {_id : storeId},
        refetchQueries: [{query: GET_MY_PICS, variables: {_id : storeId}}],
    });
    if(picLoading) return <Loading />
    if(picError) return <Error error={error} />

    const convertTo64base = (e, callback) => {
        e.persist();

        const FR= new FileReader();
        FR.addEventListener("load", async function(event) {
            const res = await callback({
                variables: { img64base: event.target.result }
            });
        }); 
          
        FR.readAsDataURL( e.target.files[0] );
    }

    return (
        <Modal isOpen={isOpen}>
            <span className="modal-close" onClick={close}>Cerrar</span>
            <div
                className="imgs"
            >
                {
                loading 
                    ? <Loading /> 
                    : <div className="imgs-container">
                        {
                            picData.getMyStore.stored_pics.length 
                                ? picData.getMyStore.stored_pics.map( (pic, i) => <div 
                                    key={`storedPic${i}`}
                                    className="img-item"
                                    onClick={() => {
                                        getImage(pic);
                                        close();
                                    }}
                                >
                                    <img src={pic} alt={`storedPic${i}`}/>
                                </div> )

                                : <div>No tienes fotos guardadas</div>
                        }
                        {
                            picData.getMyStore.stored_pics.length ? <div
                                className="img-empty"
                                onClick={() => {
                                    getImage("");
                                    close();
                                }}
                            ><img src="/icons/empty.svg" alt="empy"/></div> : null
                        }
                    </div>
                }
                <div className="img-instructions">
                    {
                    picData.getMyStore.stored_pics.length  
                        ? "Selecciona una foto" 
                        : "Sube una foto y después selecciónala"
                    }
                </div>
                <Label>
                <span className="button">Guarda una {picData.getMyStore.stored_pics.length ? 'nueva' : ''} foto</span>
                    <input 
                        className="input-file" 
                        type='file' 
                        accept='image/*'
                        onChange={(e) => convertTo64base(e, storePic)}
                    />
                </Label>
            </div>
        </Modal>
    )
}
export default StoredPics

const Modal = styled.div`
    position: fixed;
    height: ${ ({isOpen}) => isOpen ? '100vh' : '0' };
    width: 100vw;
    /* opacity:${ ({isOpen}) => isOpen ? '1' : '0' }; */
    left: 0;
    top: 0;
    background: #7a1a7b;
    z-index: 3;
    overflow: scroll;
    transition: all 0.5s;
    display: flex;
    justify-content: center;
    align-items: center;

    .modal-close {
        position: absolute;
        right: 1rem;
        top: 1rem;
        background: rgba(255,255,255, 0.5);
        font-size: 0.8rem;
        padding: 0.5rem;
        border-radius: 30px;
        cursor: pointer;
        transition: background 0.5s;

        &:hover {
            background: #fff;
        }
    }

    .imgs {
        color: rgba(255, 255, 255, 0.5);
        max-width: 600px;
        padding: 1rem;
    }

    .imgs-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;

        .img-item {
            flex: 0 1 33%;
            padding: 1rem;

            img {
                border-radius: 10px;
                box-shadow: 0 0 0 2px rgba(255,255,255, 0.5);
                cursor: pointer;
                transition: all 0.5s;

                &:hover {
                    box-shadow: 0 0 0 5px #fff;
                }
            }
        }

        .img-empty {
            flex: 0 1 33%;

            img {
                width: 50%;
            }
        }
    }

    .img-instructions {
        margin-bottom: 2rem;
    }
`
const Label = styled.label`
    cursor: pointer;

    .button {
        display: block;
        border-radius: 30px;
        padding: 0.5rem;
        border: 1px solid rgba(255,255,255, 0.5);
        margin: 1rem auto;
    }
    
    .input-file {
        display: none;
    }
`;