import { useState } from 'react';
import styled from 'styled-components';

const PasswordEye = ({ name, placeholder, onChange, onBlur, value}) => {
    const [ seeIt, setSeeIt ] = useState(false)

    function handleSee() {
        setSeeIt(!seeIt)
    }

    return (
        <>
            <input 
                type={!seeIt ? 'password' : 'text'} 
                name={name} 
                placeholder={placeholder} 
                onChange={onChange} 
                onBlur={onBlur}
                value={value} 
            />
            <Eye 
                className={`${ !seeIt ? '' : 'see'}`}
                onClick={handleSee}
            />
        </>
    )
}

const Eye = styled.span`
    position: relative;

    &:after {
        content: "";
        position: absolute;
        width: 20px;
        height: 100%;
        background: url('/icons/no-see.svg') no-repeat center;
        filter: invert(0.5);
        right: 0.5rem;
        top: 0;
        cursor: pointer;
    }

    &.see{
        &:after {
            background: url('/icons/see.svg') no-repeat center;
        }
    }
`; 

export default PasswordEye
