import { useState } from 'react'
import styled from 'styled-components'
import SectionControlBtns from './SectionControlBtns'
import  { Icon } from '../../styles/common'

function SectionMenu({ menuSection, i, swapUpSection, swapDownSection, changeStatusSection, deleteSection }) {
    const [ sectionMenuOpen, setSectionMenuOpen] = useState(false)
    
    function handleClickSectionMenu() {
        setSectionMenuOpen(prevState => !prevState)
    }

    return (
        <>
            <EditSectionBtn 
                onClick={handleClickSectionMenu}
            >
                {
                    sectionMenuOpen
                    ? <Icon size="30px">
                        <img src="/icons/delete.svg" alt="cerrar" />
                    </Icon>
                    : <Icon size="30px">
                        <img src="/icons/edit-section.svg" alt="editar seccion" />
                    </Icon>
                }
            </EditSectionBtn>
            <SectionControlBtns 
                sectionMenuOpen={sectionMenuOpen} 
                menuSection={menuSection}
                i={i}
                swapUpSection={swapUpSection}
                swapDownSection={swapDownSection}
                changeStatusSection={changeStatusSection}
                deleteSection={deleteSection}
                handleClickSectionMenu={handleClickSectionMenu}
            />
        </>
    )
}

export default SectionMenu

const EditSectionBtn = styled.span`
    position: absolute;
    background: #fbdb6d;
    border-radius: 5px 20px 10px 10px;
    right: 0;
    top: 0;
    cursor: pointer;
    width: 60px;
    height: 100%;
    display: flex;
    padding-bottom: 5px;
`;
