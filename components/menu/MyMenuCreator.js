import { useState, useEffect, useRef } from 'react'
import Router from 'next/router'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Joyride, { STATUS } from 'react-joyride'
import AddPhotoInMenu from './AddPhotoInMenu'
import { Icon } from '../../styles/common'
import { CREATE_FULL_MENU, GET_STORE_BY_SLUG, UPDATE_FULL_MENU } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
import Modal from '../common/Modal'
import PreviewAccordion from './PreviewAccordion'
import SectionMenu from './SectionMenu'


const blankPrice = {
    price_value: "",
    price_description: ""
}

const blankFood = {
    food_name: "",
    food_image: "",
    food_price: [],
    food_description: "",
    food_tags: [],
    active: true,
}

const blankSection = {
    section_title: "",
    section_description: "",
    section_footer: "",
    section_image: "",
    food_items: [],
    active: true,
};

const notCustomized = {
    page_background_color: "#fafafa",
    title_background_color: "#7a1a7b",
    title_text_color: "#ffffff",
    items_background_color: "#ffffff",
    items_text_color: "#333333",
}

const init_steps = [
    {
        content: <div><h2>Empecemos</h2><p>Ve llenando tu menú en cada paso y presiona <strong>Siguiente</strong></p></div>,
        placement: 'center',
        target: 'body',
    },
    {
        target: '.step-1',
        content: <div><p>Aquí va el nombre de tu sección</p><strong>Ejemplo: Desayunos, Bebidas, Postres, ...</strong><p>Clic para que poder editarlo</p></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-2',
        content: <div><i>(Puede estar vacío)</i><p>Escribe aquí algo que quieras decir antes de listar tu comida.</p><strong>Ejemplo: Sólo disponible de Lunes a Viernes</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-3',
        content: <div><p>Escribe el nombre de tu comida o bebida.</p><strong>Ejemplo: Cochinita Pibil, Sushi, ...</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-4',
        content: <div><i>(Puede estar vacío)</i><p>Escribe una descripción de tu comida o bebida</p><strong>Ejemplo: Receta de la casa. Suculenta y tentadora carne de cordero.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-5',
        content: <div><p>Escribe el precio de tu comida o bebida.</p></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-6',
        content: <div><i>(Puede estar vacío)</i><p>Escribe el tamaño o cantidad</p><strong>Ejemplos: Chico, Con Queso, 2/pers, 12 pzs, Extra</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-12',
        content: <div><i>(Puede estar vacío)</i><p>Escribe algo que quieras decir al final de tu sección.</p><strong>Ejemplo: Todos vienen con ensalada y café.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-13',
        content: <div><p>Agrega una foto.</p><strong>Puedes mostrar aquí una foto de tu platillo más popular.</strong></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-11',
        content: <div><p>Continúa agregando tu comida al final de esta guía.</p></div>,
        placementBeacon: 'right',
    },
    {
        target: '.step-15',
        content: <div><p>Este botón creará otra sección nueva.</p></div>,
        placementBeacon: 'right',
    },
    {
        content: <div><strong>Si necesitas ayuda, no dudes en contactarnos por el chat de messenger que se encuentra en esta pantalla.</strong></div>,
        placement: 'center',
        target: 'body',
    },
];

function MyMenuCreator({ slug, isEditor }) {
    const full_section = {...blankSection};
    full_section.food_items = [{...blankFood}];
    full_section.food_items[0].food_price = [{...blankPrice}];

    const { data: userData, loading: userLoading, error: userError } = useQuery(GET_STORE_BY_SLUG, {
        variables: { slug}
    });
    const [ menuSections, setMenuSections]  = useState([{...full_section}]);
    const [ menuPreview, setMenuPreview ] = useState(false)
    const [ hasMenu, setHasMenu ] = useState(false);
    const [ isModalPhotoOpen, toggleModalPhoto ] = useState(false)
    const [ steps, setSteps ] = useState(init_steps)
    const [ runTour, setRunTour ] = useState(false)
    const [ hasNewSection, setHasNewSection ] = useState(false)
    const [ hasNewFood, setHasNewFood ] = useState(-1)

    const myRefs= useRef({});
    const scrollToRef = (ref) => {
        window.scrollTo(0, myRefs.current[ref].offsetTop);
    };

    const [ createFullMenu, {loading, error}] = useMutation(CREATE_FULL_MENU, { 
        variables: {
            skip: !userData,
            menu: menuSections,
            ownerId: (isEditor && userData) && userData.getStoreBySlug._id,
        },
        refetchQueries: [{query: GET_STORE_BY_SLUG, variables: { slug }}],
    })

    const [ updateFullMenu, {loading: updateMenuLoading, error: updateMenuError}] = useMutation(UPDATE_FULL_MENU, {
        variables: {
            skip: !userData,
            fullMenuId: (userData && userData.getStoreBySlug.full_menu) && userData.getStoreBySlug.full_menu._id,
            menu: menuSections,
            ownerId: (isEditor && userData) && userData.getStoreBySlug._id,
        },
        refetchQueries: [{query: GET_STORE_BY_SLUG, variables: { slug }}],
    })

    useEffect ( () => {
        if((userData && userData.getStoreBySlug.full_menu) && userData.getStoreBySlug.full_menu.menu.length) {
            setMenuSections(userData.getStoreBySlug.full_menu.menu)
            setHasMenu(true)
        }
    }, [userData])

    useEffect( () => {
        if(hasNewSection) {
            scrollToRef(`ref${menuSections.length - 1}s`)
            setHasNewSection(false);
        }

        return () => setHasNewSection(false);
    }, [hasNewSection])

    useEffect( () => {
        if(hasNewFood >= 0) {
            scrollToRef(`ref${hasNewFood}s${menuSections[hasNewFood].food_items.length - 1}f`);
        }
    }, [hasNewFood])
    
    if(userLoading) return <Loading />
    if(userError) return <Error error={error} />

    const { _id } = userData.getStoreBySlug;
    const pics_allowed = 20;


    function openTour(e) {
        e.preventDefault();
        setRunTour(true);
    }

    function handleEndOfTour(data) {
        const { status, type } = data;
        const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];
        if (finishedStatuses.includes(status)) {
            setRunTour(false);
        }
    }

    function handleMenuPreview() {
        setMenuPreview(!menuPreview);
    }

    function handleOnChange(e, i) {
        e.persist();
        const { name, value } = e.target;
        
        const prevOne = menuSections;
        prevOne[i][name] = value;
        setMenuSections([...prevOne])
    }

    function handleOnChangeFood(e, i, j) {
        e.persist();
        const { name, value } = e.target;
        const prevOne = menuSections;
        prevOne[i].food_items[j][name] = value;
        setMenuSections([...prevOne])
    }

    function handleOnChangeTag(name, i, j) {
        const prevOne = menuSections;
        let tags = prevOne[i].food_items[j].food_tags;
        const already = tags.includes(name);
        
        if(!already) {
            tags.push(name);
        } else {
            prevOne[i].food_items[j].food_tags = tags.filter( tag => tag !== name);
        }
        setMenuSections([...prevOne])
    }

    function handleOnChangePrice(e, i, j, k) {
        e.persist();
        const { name, value } = e.target;
        const prevOne = menuSections;
        prevOne[i].food_items[j]["food_price"][k][name] = value;
        setMenuSections([...prevOne])
    }

    function addPhoto(pic, i, j) {
        const countedPhotos = calculatePhotos();
        const prevOne = menuSections;
        
        
        if(countedPhotos || prevOne[i].food_items[j]["food_image"]) {
            prevOne[i].food_items[j]["food_image"] = pic;        
            setMenuSections([...prevOne])
        }else{
            toggleModalPhoto(true)
        }
    };

    function addPhotoSection(pic, i) {
        const countedPhotos = calculatePhotos();
        const prevOne = menuSections;
        if(countedPhotos || prevOne[i].section_image) {
            prevOne[i].section_image = pic;        
            setMenuSections([...prevOne]);
        }else{
            toggleModalPhoto(true)
        }
    };

    function handleToggleModalPhoto() {
        toggleModalPhoto(false)
    }
    
    function calculatePhotos() {
        const arr = JSON.stringify(menuSections);
        const countPhotos = (arr.match(/https:\/\/res.cloudinary.com/g) || []).length;
        return countPhotos < pics_allowed;
    }

    function addPrice(i,j) {
        const menu = [...menuSections];
        menu[i].food_items[j].food_price = [ ...menu[i].food_items[j].food_price, {...blankPrice}];
        // console.log(menu[i].food_items[j].food_price);
        setMenuSections([...menu]);
    }

    function deletePrice(i,j,k) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar este precio?');
        if(confirmDelete){
            const menu = menuSections;
            menu[i].food_items[j].food_price = menu[i].food_items[j].food_price.filter( (p,i) => i !== k)
            setMenuSections([...menu]);
        }
        
    }

    function addFoodItem(i){
        const menu = [...menuSections];
        menu[i].food_items= [...menu[i].food_items, {...blankFood}];
        menu[i].food_items[menu[i].food_items.length - 1].food_price= [...menu[i].food_items[menu[i].food_items.length - 1].food_price, {...blankPrice}];
        menu[i].food_items[menu[i].food_items.length - 1].food_tags = [];
        setMenuSections([...menu])
        handleMenuPreview();
        setHasNewFood(i);
        // scrollToRef(`ref${i}s${menu[i].food_items.length}f`);
    }

    function deleteFoodItem(i,j) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar esta comida o bebida?');
        if(confirmDelete){
            const menu = [...menuSections];
            menu[i].food_items = menu[i].food_items.filter( (f,i) => i !== j)
            setMenuSections([...menu]);
        }
    }

    function addSection(){
        const menu = [...menuSections];
        const newMenu = [...menu, {...full_section}]
        setMenuSections(newMenu);
        handleMenuPreview();
        setHasNewSection(true);
    }

    function deleteSection(i) {
        const confirmDelete = window.confirm('¿Estás seguro que quieres borrar esta sección?');
        if(confirmDelete){
            let menu = [...menuSections];
            menu = menu.filter( (s,index) => index !== i)
            setMenuSections([...menu]);
        }
    }

    function swapUpSection(i) {
        let menu = menuSections;
        [menuSections[i], menuSections[i - 1]] = [menuSections[i - 1], menuSections[i]];
        setMenuSections([...menu]);
    }

    function swapDownSection(i) {
        let menu = menuSections;
        [menu[i], menu[i + 1]] = [menu[i + 1], menu[i]];
        setMenuSections([...menu]);
    }

    function swapUpFood(i,j) {
        let menu = menuSections;
        let currFood = menu[i].food_items;
        [currFood[j], currFood[j - 1]] = [currFood[j - 1], currFood[j]];
        setMenuSections([...menu]);
    }

    function swapDownFood(i,j) {
        let menu = menuSections;
        let currFood = menu[i].food_items;
        [currFood[j], currFood[j + 1]] = [currFood[j + 1], currFood[j]];
        setMenuSections([...menu]);
    }

    function changeStatusFood(i,j) {
        const menu = menuSections;
        if(menu[i].food_items[j].active === null) {
            menu[i].food_items[j].active = true;
        }
        menu[i].food_items[j].active = !menu[i].food_items[j].active;
        setMenuSections([...menu]);
    }

    function changeStatusSection(i) {
        const menu = menuSections;
        if(menu[i].active === null) {
            menu[i].active = true;
        }
        menu[i].active = !menu[i].active;
        setMenuSections([...menu]);
    }

    async function handleClickPublishMenu() {
        const res = await createFullMenu();
        Router.push(`/${slug}`)
    }
    async function handleClickUpdateMenu() {
        const res = await updateFullMenu();
        Router.push(`/${slug}`)
    }

    async function saveBeforeRedirectToPlans() {
        const res = hasMenu ? await updateFullMenu() : await createFullMenu();
        Router.push('/negocios/ver-planes'); 
    }

    const InputsGroups = menuSections.map((menuSection, i) => (
        <div key={`sectkey${i}`}>
            <FieldsetAndPreviewWrap>
                <FieldsetWrap>
                    <Fieldset 
                        className={menuSection.active === null || menuSection.active === true ? '' : 'disabled'}
                        ref={(el) => {
                                            
                            myRefs.current[`ref${i}s`] = el
                        }}
                    >
                        <div 
                            className="section-title"
                        >
                            <input 
                                type="text" 
                                value={menuSection.section_title} 
                                name="section_title" 
                                placeholder="Nombre de la Sección"
                                onChange={(e) =>handleOnChange(e,i)} 
                                className="step-1"
                            />
                            <SectionMenu 
                                menuSection={menuSection} 
                                i={i}
                                swapUpSection={swapUpSection}
                                swapDownSection={swapDownSection}
                                changeStatusSection={changeStatusSection}
                                deleteSection={deleteSection}
                            />
                        </div>
                        <textarea 
                            name="section_description" 
                            rows="3" 
                            value={menuSection.section_description} 
                            placeholder="Escribe aquí un mensaje opcional al inicio de ésta sección"
                            onChange={(e) =>handleOnChange(e,i)}
                            className="step-2"
                        ></textarea>

                        <div className="food-items">
                            {
                                menuSection.food_items.map( (foodItem, j) => (
                                    <div 
                                        key={`foodkey${i}${j}`}
                                        className="food-item"
                                        ref={(el) => {
                                            
                                            myRefs.current[`ref${i}s${j}f`] = el
                                        }}
                                    >
                                        <div className={`food-name ${foodItem.active === null || foodItem.active === true ? '' : 'disabled'}`}>
                                            <div className="food-left">
                                                <input
                                                    type="text"
                                                    name="food_name"
                                                    value={menuSection.food_items[j].food_name}
                                                    placeholder="Alimento ó bebida"
                                                    onChange={(e) =>handleOnChangeFood(e,i,j)}
                                                    className="food-name-input step-3"
                                                />
                                                <textarea 
                                                    name="food_description" 
                                                    rows="3" 
                                                    value={menuSection.food_items[j].food_description} 
                                                    placeholder="Descripción Opcional"
                                                    onChange={(e) =>handleOnChangeFood(e,i,j)}
                                                    className="step-4"
                                                ></textarea>
                                                <div className="extras">
                                                    {
                                                        menuSection.food_items[j].food_price.map( (price, k) => (
                                                            <div key={`priceKey${i}${j}${k}`} className="price-wrap">
                                                                <input
                                                                    type="text"
                                                                    name="price_value"
                                                                    value={menuSection.food_items[j].food_price[k].price_value}
                                                                    placeholder="$ Precio"
                                                                    onChange={(e) =>handleOnChangePrice(e,i,j,k)}
                                                                    className="price-value"
                                                                    className="step-5"
                                                                />
                                                                <input
                                                                    type="text"
                                                                    name="price_description"
                                                                    value={menuSection.food_items[j].food_price[k].price_description}
                                                                    placeholder="Ej: Ch, 4pz"
                                                                    onChange={(e) =>handleOnChangePrice(e,i,j,k)}
                                                                    className="price-desc"
                                                                    className="step-6"
                                                                />
                                                                {
                                                                    menuSection.food_items[j].food_price.length > 1 &&
                                                                    <span onClick={() => deletePrice(i,j,k)} style={{ width: '60px', marginRight: '-3.4rem', marginLeft: '1rem', padding: '0.5rem', zIndex: '0'  }}><img src="/icons/delete.svg" alt="borrar"/></span>
                                                                }
                                                                
                                                            </div>
                                                        ))
                                                    }
                                                    <ButtonAddMenu onClick={ () => addPrice(i,j)}>
                                                        <span className="icon"><img src="/icons/price.svg" alt="+"/></span>
                                                        <span className="step-7">Otro Precio</span>
                                                    </ButtonAddMenu>
                                                    
                                                    <div className="tag-wrap" >
                                                        <div className="tag-wrap-title step-8">Selecciona si es:</div>
                                                            <span
                                                                className={`tag ${menuSection.food_items[j].food_tags.includes('Popular') && 'selected'}`}
                                                                onClick={ () => handleOnChangeTag("Popular", i, j)}
                                                            >
                                                                Popular
                                                            </span>
                                                            <span 
                                                                className={`tag ${menuSection.food_items[j].food_tags.includes('Picoso') && 'selected'}`} 
                                                                onClick={ () => handleOnChangeTag("Picoso", i, j)}
                                                            >
                                                                Picoso
                                                            </span>
                                                            
                                                            <span 
                                                                className={`tag ${menuSection.food_items[j].food_tags.includes('Vegetariano') && 'selected'}`} 
                                                                onClick={ () => handleOnChangeTag("Vegetariano", i, j)}
                                                            >
                                                                Vegetariano
                                                            </span>
                                                            <span 
                                                                className={`tag ${menuSection.food_items[j].food_tags.includes('Saludable') && 'selected'}`} 
                                                                onClick={ () => handleOnChangeTag("Saludable", i, j)}
                                                            >
                                                                Saludable
                                                            </span>
                                                            
                                                    </div>
                                                        
                                                </div>
                                            </div>
                                            <div className="step-9">
                                                    <AddPhotoInMenu addPhotoToParentState={addPhoto} i={i} j={j} hasImage={menuSection.food_items[j].food_image} storeId={ isEditor && _id } />
                                            </div>
                                        </div>
                                        <div className="food-name-clone">{menuSection.food_items[j].food_name}</div>

                                        <div className="edit-food">
                                            {
                                                j > 0 &&
                                                <ActionButton onClick={() => swapUpFood(i,j)}>
                                                    <span>
                                                        <Icon size="20px">
                                                            <img src="/icons/arrow-up.svg" alt="Subir" />
                                                        </Icon>
                                                    </span>
                                                    <span className="caption">Subir</span>
                                                </ActionButton>
                                                
                                            }
                                            {
                                                j + 1 < menuSections[i].food_items.length &&
                                                <ActionButton onClick={() => swapDownFood(i,j)}>
                                                    <span>
                                                        <Icon size="20px">
                                                            <img src="/icons/arrow-down.svg" alt="Subir" />
                                                        </Icon>
                                                    </span>
                                                    <span className="caption">Bajar</span>
                                                </ActionButton>
                                            }

                                            <ActionButton onClick={() => changeStatusFood(i,j)}>
                                                <span>
                                                    {
                                                    foodItem.active || foodItem.active === null
                                                    ? <Icon size="20px"><img src="/icons/no-see.svg" alt="Desactivar" /></Icon>
                                                    : <Icon size="20px"><img src="/icons/see.svg" alt="Activar" /></Icon>
                                                    }
                                                </span>
                                                <span className="caption">
                                                    {
                                                        foodItem.active || foodItem.active === null
                                                        ? 'Esconder'
                                                        : 'Mostrar'
                                                    }
                                                </span>
                                            </ActionButton>
                                            <div className="step-10">
                                                <ActionButton onClick={() => deleteFoodItem(i,j)}>
                                                    <span>
                                                        <Icon size="20px">
                                                            <img src="/icons/rubbish.svg" alt="Borrar" />
                                                        </Icon>
                                                    </span>
                                                    <span className="caption">Borrar</span>
                                                </ActionButton>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>

                        <textarea 
                            name="section_footer" 
                            rows="3"
                            placeholder="Escribe aquí un mensaje opcional al final de ésta sección"
                            value={menuSection.section_footer} 
                            onChange={(e) =>handleOnChange(e,i)}
                            className="section_footer step-12"
                        ></textarea>

                        <div className="step-13"><AddPhotoInMenu addPhotoToParentState={addPhotoSection} i={i} hasImage={menuSection.section_image} storeId={ isEditor && _id } /></div>

                    </Fieldset>
                </FieldsetWrap>
            </FieldsetAndPreviewWrap>
        </div>
    ));


    return (
        <GridMenuCreator>
            <Joyride
                steps={steps}
                styles={{
                    options: {
                      overlayColor: 'rgba(0, 0, 0, 0.5)',
                      primaryColor: '#7a1a7b',
                      width: 400,
                      zIndex: 1,
                    }
                }}
                continuous= {true}
                locale= {{ back: 'Regresar', close: 'Cerrar', last: 'Fin', next: 'Siguiente', skip: 'Cerrar' }}
                showProgress={true}
                run={runTour}
                spotlightClicks={true}
                callback={handleEndOfTour}
            />
            <div>
                <p>
                    <button 
                        className="secondary" 
                        onClick={openTour} 
                        disabled={runTour}
                    >¿Cómo editar mi menú?</button>
                </p>
                
                {InputsGroups}

                <Modal isOpen={isModalPhotoOpen} closeModal={handleToggleModalPhoto}>
                    <span style={{fontSize: '2rem'}}>Oops!</span>
                    <div style={{ fontSize: '1.5rem' }}></div>
                    <p>Con éste plan sólo puedes tener <strong>{pics_allowed} foto{pics_allowed > 1 && 's'}</strong> en tu menú.</p>
                </Modal>
                
                <MainBtns>
                    <PreviewMenuWrap open={menuPreview}>
                        <PreviewAccordion 
                            menuSections={menuSections} 
                            scrollToRef={scrollToRef} 
                            handleMenuPreview={handleMenuPreview} 
                            addSection={addSection}
                            addFoodItem={addFoodItem}
                        />
                    </PreviewMenuWrap>
                    

                    <button
                        className="secondary left"
                        onClick={ handleMenuPreview }
                    >
                        <Icon>
                            <img src="/icons/hierarchy.svg" alt="indice" />
                        </Icon>
                    </button>
                    <button 
                        className="right"
                        onClick={hasMenu ? handleClickUpdateMenu : handleClickPublishMenu}
                    >
                        <Icon><img src="/icons/updated.svg" alt="actualizar" /></Icon>
                        <span>Guardar</span>
                    </button>
                </MainBtns>
            </div>
        </GridMenuCreator>
    )
}

export default MyMenuCreator;

const GridMenuCreator = styled.div`
    padding: 0;

    button.react-joyride__beacon[type="button"]:after {
        content: "?";
        color: #fff;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        font-family: Helvetica;
        font-size: 0.8rem;
    }
`;

const ButtonAddMenu = styled.button`
    background: ${ ({section}) => section? '#333' : '#fafafa'};
    border: 1px solid #e6e7e8;
    font-family: Helvetica, sans-serif;
    font-size: 0.8rem;
    color: ${ ({section}) => section? '#fff' : '#333'};
    margin: 0.5rem 0 0.5rem;
    
    span {
        display: inline-block;
        vertical-align: middle;
    }
    .icon {
        width: 20px;
        margin-right: 0.5rem;
    }
`

const MainBtns = styled.div`
    .left {
        box-shadow: 0 0 11px 5px rgba(0,0,0,0.2);
        z-index: 2;
        position: fixed;
        left: 0;
        bottom: 0;
        padding: 10px;
        width: 80px;
        height: 80px;
        border-radius: 0 30px 0 0;
    }

    .right {
        box-shadow: 0 0 11px 5px rgba(0,0,0,0.2);
        /* border-radius: 30px 0 0 0; */
        z-index: 1;
        position: fixed;
        right: 0;
        bottom: 0;
        font-size: 1.25rem;
        line-height: 1.2rem;
        padding: 10px;
        border-radius: 30px 0 0 0;
        height: 80px;
    }
`;

const FieldsetAndPreviewWrap = styled.div`
    /* display: flex;
    justify-content: center; */
    margin-bottom: 5rem;
`;

const FieldsetWrap = styled.div`
    /* overflow: hidden;
    flex: 100%;
    width: 0;

    @media all and (min-width:640px) {
        flex: 1 1 100%;
        width: auto;
    } */
`;

const Fieldset = styled.fieldset`
    border: 10px solid #7a1a7b;
    border-bottom: 6px solid #7a1a7b;
    border-radius: 30px;
    max-width: 400px;
    /* width: 100%; */
    padding: 0;
    margin: 0 0 1.5rem;
    background-color: #fff;

    .section-title {
        position: sticky;
        left: 0;
        top: 0;
        margin-bottom: 2rem;
        z-index:1;

        input {
            background: #7a1a7b;
            color: #fff;
            border-radius: 0 0 30px 0;
            box-shadow: 0 -10px 0px 10px rgba(0,0,0,0.2) inset;
            text-align: center;
            padding-right: 60px;
            font-size: 1.8rem;
            height: 60px;
        }
    }

    input, textarea {
        border: 0;
        color: inherit;
        border-radius: 10px;
        background: transparent;
        font-size: 1rem;
        width: 100%;
        padding: 0 0.5rem;
        font-family: Helvetica, sans-serif;
        min-width: initial;
        margin: 0 0 0.5rem 0;
    }
    .price-wrap {
        display: flex;
    }
    
    .tag-wrap {
        font-size: 0.8rem;
        margin: 1rem 0;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        cursor: pointer;
        
        .tag-wrap-title {
            flex: 100%;
            margin-bottom: 1rem;
            text-align: left;
            padding-left: 0.8rem;
        }
    }
    .tag {
        margin-left: 0.5rem;
        padding: 0.5rem;
        background: #fff;
        border: 1px solid #e6e7e8;
        border-radius: 30px;
        margin-bottom: 0.5rem;
        
        &.selected {
            background: #7a1a7b;
            color: #fff;
        }
    }

    input.price-value {
        width: 5rem;
    }

    fieldset {
        padding: 0;
        border: 0;
    }

    .food-name {
        display: flex;
        
        justify-content: space-between;

        .food-left {
            flex: 1 0 75%;
            padding-right: 0.5rem;
        }

        .food-name-input {
            font-size: 1.5rem;
        }

        .extras {
            text-align: left;
        }

        &.disabled {
            & > * {
                opacity: 0.3;
            }
            
            &:before {
                content: "";
                width: 100px;
                height: 80px;
                background: url('/icons/hiding.svg') no-repeat center;
                position: absolute;
                top: 30%;
                left: 50%;
                transform: translateX(-50%);
                background-size: cover;
            }
            &:after {
                content: "Este producto no se mostrará en tu menú";
                position: absolute;
                top: calc(30% + 80px);
                left: 50%;
                transform: translateX(-50%);
                background: #7a1a7b;
                border-radius: 30px;
                color: #fff;
                padding: 0.5rem 1rem;
            }
        }
    }

    .food-items {
        /* margin: 0 -0.5rem 0.5rem -0.5rem;
        padding: 0.5rem; */
    }

    .food-item {
        border-radius: 30px 16px 30px 30px;
        padding: 1.5rem 10px 0.5rem;
        margin-bottom: 8rem;
        box-shadow: 0 -5px 10px 2px rgba(0,0,0,0.1);
        background: linear-gradient(0deg, #f5f5f5 0%, #fff 100%);
        position: relative;

        .edit-food {
            bottom: -1.8rem;
            right: 0;
            width: 100%;
            padding: 10px;
            position: absolute;
            /* background: #fbdb6d; */
            border-radius: 20px;
            margin-bottom: -46px;
            display: flex;
            justify-content: center;
        }
    }

    .section_footer {
        margin: 2.5rem 0 1.5rem;
    }

    .food-name-clone {  
        color: #999;
    }

    &.disabled {
        position: relative;

        & > * {
            opacity: 0.4;
        }

        &:before {
            content: "";
            width: 100px;
            height: 100px;
            background: url('/icons/ninja-hiding.svg') no-repeat;
            background-size: cover;
            top: 30%;
            left: 50%;
            transform: translateX(-50%);
            position: absolute;
            z-index: 1;
        }

        &:after {
            content: "Esta sección no se mostrará en tu menú.";
            background: #fbdb6d;
            border-radius: 30px;
            padding: 0.5rem 1rem;
            top: calc(30% + 100px);
            left: 50%;
            transform: translateX(-50%);
            position: absolute;
        }
    }
`;

const ActionButton = styled.button`
    font-family: Arial, Helvetica, sans-serif;
    display: flex;
    flex-direction: column;
    font-size: 0.85rem;
    align-items: center;
    background: none;
    color: #999;
    padding: 10px;
    border-radius: 0;
    
    .caption {
        margin-top: 0.5rem;
    }
`;

const PreviewMenuWrap = styled.div`
    position: fixed;
    background: #fbdb6d;
    width: 100%;
    height: ${ ({open}) => open ? '100%' : '0%'};
    bottom: 0;
    left: 0;
    z-index: 2;
    transition: height 0.4s;
    overflow: scroll;
`;

