import { useRef, useState, useEffect } from 'react'
import styled from 'styled-components'
import { Icon } from '../../styles/common'

function PreviewAccordion({menuSections, scrollToRef, handleMenuPreview, addSection, addFoodItem }) {
    
    function handleSectionClick(i) {
        handleMenuPreview();
        scrollToRef(`ref${i}s`);
    }
    
    function handleFoodClick(i,j) {
        handleMenuPreview();
        scrollToRef(`ref${i}s${j}f`)
    }

    return (
        <PreviewSection>
            { menuSections.map( (menuSection, i) => (
                <div 
                    key={`preview_section${i}`} 
                    className={`preview-section ${menuSection.active === false ? 'disabled' : ''}`}
                >
                    <div 
                        className="preview-header"
                        onClick={() => handleSectionClick(i)}
                    >
                        {menuSection.section_title}
                    </div>
                    <div 
                        className="accordion"
                    >
                        <div className="preview-food">
                            {menuSection.food_items.map( (f,j) => (
                                <span 
                                    key={`f.preview_food_name${i}${j}`} 
                                    className={`preview-food-name ${f.active === false ? 'disabled' : ''}`}
                                    onClick={() => handleFoodClick(i,j)}
                                >
                                    {f.food_name}
                                </span>
                            ))}
                            <span
                                className="preview-food-name add-button"
                                onClick={() => addFoodItem(i)}
                            >
                                <Icon>
                                    <img src="/icons/add-menu.svg" alt="+"/>
                                </Icon>
                                <span>Agregar Comida</span>
                            </span>
                        </div>
                    </div>
                </div>
            ))}
            
            <div
                className="sticky-button"
            >
                <span
                    className="button"
                    onClick={addSection}
                >
                    <Icon>
                        <img src="/icons/add-section.svg" alt="agregar seccion" />
                    </Icon>
                    <span>Nueva Sección</span>
                </span>
            </div>
        </PreviewSection>
    )
}

export default PreviewAccordion


const PreviewSection = styled.div`
    padding: 1rem 1rem 200px;

    .preview-section {
        margin-bottom: 2rem;

        &.disabled {
            opacity: 0.3;
        }
    }

        .preview-header {
            text-align: left;
            font-size: 2rem;
            font-weight: bold;
            cursor: pointer;
        }

        .accordion {
            border-left: 1px dashed #7a1a7b;
            padding-left: 20px;
        }

        .preview-food {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
        }

        .preview-food-name {
            border-radius: 30px;
            cursor: pointer;
            position: relative;
            font-size: 1.2rem;
            padding: 0.5rem 1rem;
            margin: 0.1rem 0;
            margin: 0.4rem 0;
            border: 1px dashed #7a1a7b;
            /* box-shadow: 0 0 0px 1px rgba(0,0,0,0.4); */
            position: relative;
            text-align: left;

            &:after {
                content: "";
                height: 50%;
                width: 20px;
                position: absolute;
                left: -20px;
                bottom: 0;
                border-top: 1px dashed #7a1a7b;
            }

            &.add-button {
                background: rgba(255,255,255,0.4)
            }

            &.disabled {
                opacity: 0.3;
            }
        }

        .sticky-button {
            position: sticky;
            bottom: 10px;
            left: 100%;
            width: 200px;
            display: flex;
            justify-content: flex-end;

            .button {
                background: #000;
            }
        }
`;
