import Link from 'next/link'
import { useQuery } from "@apollo/react-hooks"
import { GET_CURRENT_USER } from '../../queries'
import DeleteMenu from './DeleteMenu'
import { Icon, GridCols, ButtonLike } from '../../styles/common'
import Loading from '../common/Loading'
import Error from '../common/Error'

function EditMenuNav({ menuId, ownerId }) {
    const {loading, error, data} = useQuery(GET_CURRENT_USER);
    if(loading) return <Loading />
    if(error) return <Error error={error}/>
    
    return (
        <>
            {(data.getCurrentUser && (ownerId == data.getCurrentUser._id))
                ? <GridCols align={'flex-end'} padding={'0 1rem'}>
                    <div className="col">
                        <Link href="/negocios/editar-menu/[menuId]" as={`/negocios/editar-menu/${menuId}`} passHref>
                            <ButtonLike>
                                <Icon size={'12px'}><img src="/icons/edit.svg"/></Icon>
                                &nbsp;Editar
                            </ButtonLike>
                        </Link>
                    </div>
                    <div className="col">
                        <DeleteMenu menuId={menuId} ownerId={ownerId}/>
                    </div>
                </GridCols>
                : null
            }
        </>
    
    )
}

export default EditMenuNav
