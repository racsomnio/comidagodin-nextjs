import { useCallback, useEffect, useState } from 'react' 
import styled from 'styled-components'
import Router from 'next/router'
import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_STORE_BY_SLUG, UPDATE_CUSTOMIZED_FULL_MENU } from '../../queries'
import FullMenuCard from '../store/FullMenuCard'
import { debounce } from 'lodash'
import { Divider } from '../../styles/common'
import Loading from '../common/Loading'
import Error from '../common/Error'

const notCustomized = {
    page_background_color: "#fafafa",
    title_background_color: "#7a1a7b",
    title_text_color: "#ffffff",
    items_background_color: "#ffffff",
    items_text_color: "#333333",
}

const exampleSection = {
    section_title: "Categoría de Menú",
    section_description: "Aquí se puede poner el horario ó detalles en general de una categoría",
    section_footer: "Ejemplo de un mensaje al final",
    section_image: "",
    food_items: [
        {
            food_name: "Comida Ejemplo 1",
            food_image: "",
            food_price: [{
                price_value: "12",
                price_description: ""
            }],
            food_description: "Descripcíon de la comida, puede decir las opciones que tiene este platillo también",
            food_tags: [],
            active: true,
        }
    ]
};

const PickColor = ({slug}) => {
    const { loading, error, data } = useQuery(GET_STORE_BY_SLUG, {
        variables: { slug}
    });
    const [color, setColor] = useState(notCustomized)
    const delayedMutation = useCallback(debounce(updateState, 500), []);
    const [updateCustomized, {loading: customizedLoading}] = useMutation(UPDATE_CUSTOMIZED_FULL_MENU, {
        variables: {
            skip: !data,
            fullMenuId: (data && data.getStoreBySlug.full_menu) && data.getStoreBySlug.full_menu._id,
            customized: color,
        },
        refetchQueries: [{query: GET_STORE_BY_SLUG, variables: { slug }}],
    })

    useEffect ( () => {
        if((data && data.getStoreBySlug.full_menu)) {
            const fromDB = data.getStoreBySlug.full_menu.customized;
            let replaceColor = {...color};
            Object.keys(fromDB).forEach( (custom, i) => {
                if(fromDB[custom]) replaceColor = {...replaceColor, [custom]: fromDB[custom]}
            })
            setColor(replaceColor)
        }
    }, [data])

    if(loading) return <Loading />
    if(error) return <Error error={error} />

    function changeColor(e) {
        const {name, value} = e.target;
        delayedMutation(name, value);
    }

    function updateState(name, value) {
        setColor(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    function handleReset() {
        setColor(notCustomized)
    }

    async function handleClickUpdateCustomized() {
        const res = await updateCustomized();
        Router.push(`/${slug}`)
    }

    return (
        <div>
            <GridColorPick>
                <PreviewWrap color={color}>
                    <FullMenuCard menuSection={exampleSection} customizeInputs={color} />
                </PreviewWrap>
                <Colors>
                    <p>Cambia los colores de tu ménu:</p>
                    <div>
                        <input type="color" 
                            id="pbc"
                            name="page_background_color" 
                            value={color.page_background_color} 
                            onChange={changeColor}
                        />
                        <label htmlFor="pbc">Página</label>
                    </div>
                    <div>
                        <input type="color" 
                            id="tbc"
                            name="title_background_color" 
                            value={color.title_background_color} 
                            onChange={changeColor}
                        />
                        <label htmlFor="pbc">Categoría</label>
                    </div>
                    <div>
                        <input type="color" 
                            id="ttc"
                            name="title_text_color" 
                            value={color.title_text_color} 
                            onChange={changeColor}
                        />
                        <label htmlFor="ttc">Texto de la categoría</label>
                    </div>
                    <div>
                        <input type="color" 
                            id="ibc"
                            name="items_background_color" 
                            value={color.items_background_color} 
                            onChange={changeColor}
                        />
                        <label htmlFor="ibc">Sección</label>
                    </div>
                    <div>
                        <input type="color" 
                            id="itc"
                            name="items_text_color" 
                            value={color.items_text_color} 
                            onChange={changeColor}
                        />
                        <label htmlFor="itc">Texto de la sección</label>
                    </div>

                    <span className="reset-btn" onClick={handleReset}>Reiniciar</span>
                </Colors>
                
                {/* <style jsx global>{``}</style> */}
            </GridColorPick>

            <Divider />
            <button 
                className="secondary"
                onClick={handleClickUpdateCustomized}
            >
                Guardar Cambios
            </button>
        </div>
        
    );
}
export default PickColor;

const GridColorPick = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;

    input {
        min-width: 80px;
        padding: 5px;
        border-radius: 10px;
        margin: 0.5rem;
    }
    label {
        font-size: 0.8rem;
    }

    .reset-btn {
        border-radius: 20px;
        background: #fff;
        border: 1px solid #e6e7e8;
        padding: 0.5rem 1rem;
        font-size: 0.8rem;
        margin: 0.5rem 0.5rem 2rem;
        display: inline-block;
        width: 80px;
        cursor: pointer;
    }
`;

const Colors = styled.div`
    text-align: left;
`;

const PreviewWrap = styled.div`
    width: 320px;
    font-size: 0.8rem;
    padding: 2rem 2rem 0;
    background: ${({color}) => color.page_background_color};
    border-radius: 10px;
    transition: background 0.3s;
    margin: 0 1rem;

    .preview-header {
        background: ${({color}) => color.title_background_color};
        color: ${({color}) => color.title_text_color};
        transition: background 0.3s, color 0.3s;
    }

    .preview-section {
        background: ${({color}) => color.items_background_color};
        color: ${({color}) => color.items_text_color};
        transition: background 0.3s, color 0.3s;
    }
`;