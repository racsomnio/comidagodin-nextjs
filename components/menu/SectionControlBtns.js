import styled from "styled-components";
import { Icon } from '../../styles/common'

const SectionControlBtns = ({ sectionMenuOpen, menuSection, i, swapUpSection, swapDownSection, changeStatusSection, deleteSection, handleClickSectionMenu }) => {
    function closeAfterCallback(i, callback) {
        callback(i)
        handleClickSectionMenu();
    }

    return (
        <SectionMenu open={sectionMenuOpen}>
            {
                i > 0 &&
                <SectionBtn onClick={() => closeAfterCallback(i,swapUpSection)}>
                    <Icon size="30px">
                        <img src="/icons/arrow-up.svg" alt="Subir Seccion" />
                    </Icon>
                    <span className="caption">Subir Sección</span>
                </SectionBtn>
            }
            {
                // i + 1 < menuSections.length &&
                <SectionBtn onClick={() => closeAfterCallback(i,swapDownSection)}>
                    <Icon size="30px">
                        <img src="/icons/arrow-down.svg" alt="Bajar Seccion" />
                    </Icon>
                    <span className="caption">Bajar Sección</span>
                </SectionBtn>
            }
            <SectionBtn onClick={() => closeAfterCallback(i,changeStatusSection)}>
                {
                    menuSection.active || menuSection.active === null
                    ? <Icon size="30px"><img src="/icons/no-see.svg" alt="Desactivar" /></Icon>
                    : <Icon size="30px"><img src="/icons/see.svg" alt="Activar" /></Icon>
                }
                <span className="caption">
                    {
                        menuSection.active || menuSection.active === null
                        ? 'Esconder Sección'
                        : 'Mostrar Sección'
                    }
                </span>
            </SectionBtn>
            <SectionBtn onClick={() => closeAfterCallback(i,deleteSection)}>
                <Icon size="30px">
                    <img src="/icons/rubbish.svg" alt="Borrar" />
                </Icon>
                <span className="caption">Borrar Sección</span>
            </SectionBtn>
        </SectionMenu>
    )
}
export default SectionControlBtns

const SectionMenu = styled.div`
    position: absolute;
    width: 100%;
    height: ${({open}) => open ? '150px' : '0'};
    top: 60px;
    left: 0;
    background: #fbdb6d;
    border-radius: 0 0 10px 10px;
    box-shadow: 0px 14px 20px -15px rgba(0,0,0,0.6);
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;  
    transition: height 0.3s;
    overflow: hidden;  
`;


const SectionBtn = styled.span`
    flex: 0 0 50%;
    padding: 1rem;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    cursor: pointer;
    
    ${Icon} {
        flex: 0 0 30px;
        margin: 0%;
    }

    & .caption {
        text-align: left;
    }
`