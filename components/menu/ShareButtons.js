import { Icon, GridCols } from '../../styles/common';
import {
    EmailShareButton,
    FacebookShareButton,
    LinkedinShareButton,
    PinterestShareButton,
    TwitterShareButton,
    WhatsappShareButton,
  } from "react-share";

const ShareButtons = ({url, text, media}) => {
    const obj = [
        {
            socialNetwork: FacebookShareButton,
            icon: 'facebook',
            alt: 'fb',
            props: {
                quote: text,
                // hashtag: null
            }
        },
        {
            socialNetwork: TwitterShareButton,
            icon: 'twitter',
            alt: 'tt',
            props: {
                title: text,
                // via: null,
                // hashtags: null,
                // related: null
            }
        },
        {
            socialNetwork: WhatsappShareButton,
            icon: 'whatsapp',
            alt: 'wa',
            props: {
                title: text,
            }
        },
        // {
        //     socialNetwork: PinterestShareButton,
        //     icon: 'pinterest',
        //     alt: 'p',
        //     props: {
        //         media: media[0],
        //         description: text
        //     }
        // },
        {
            socialNetwork: LinkedinShareButton,
            icon: 'linkedin',
            alt: 'li',
            props: {
                title: text,
                // summary: null,
                // source : null
            }
        },  
    ];

    return (
        <div style={{ marginTop: '2rem' }}>
            <p style={{ fontSize: '0.8rem', textAlign: 'center' }}>Compártelo aquí:</p>
            
            <GridCols align="center" colPadding="0 2rem 1rem 0 " >
                {
                    obj.map( item => (
                        <div key={item.alt} className="col">
                            { React.createElement(
                                item.socialNetwork,
                                {   
                                    url: `https://comidagodin.com${url}`,
                                    style: { borderRadius: '0' },
                                    ...item.props
                                },
                                <Icon>
                                    <img src={`/icons/${item.icon}.svg`} alt={item.alt}/>
                                </Icon>
                            )}
                        </div>
                    ))
                }
                
            </GridCols>
        </div>
        
    )
}

export default ShareButtons;