import { useState, useEffect } from 'react'
import { useQuery } from "@apollo/react-hooks"
import styled from 'styled-components'
import { GET_MENUS } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
import Card from './Card'
import { swap_it } from '../../utils'

function MenuList({meal}) {
    const [coords, setCoords] = useState(localStorage.getItem('coords'));

    useEffect( () => {
        const options = {maximumAge:10000, timeout:5000, enableHighAccuracy: true};
        navigator.geolocation.watchPosition(
            myLocation,
            err => console.warn('ERROR(' + err.code + '): ' + err.message) 
            , options);
    }, [navigator.geolocation])

    function myLocation(pos) {
        const lat = pos.coords.latitude;
        const long = pos.coords.longitude;
        const prevCoords = localStorage.getItem('coords')
        const newCoords = `${long},${lat}`

        const splitPrevCoords = prevCoords.split(',');
        const prevLat = splitPrevCoords[1];
        const prevLong = splitPrevCoords[0];
        
        // calculate distance btw 2 cardinal points
        const φ1 = prevLat * Math.PI/180, φ2 = lat * Math.PI/180, Δλ = (long-prevLong) * Math.PI/180, R = 6371e3;
        const d = Math.acos( Math.sin(φ1)*Math.sin(φ2) + Math.cos(φ1)*Math.cos(φ2) * Math.cos(Δλ) ) * R;
        if(d > 60) {
            setCoords(newCoords);
            localStorage.setItem('coords', newCoords)
        }
    }

    const swapMeal = swap_it[meal];
    const { loading, error, data, fetchMore } = useQuery(GET_MENUS, {
        variables: { coordinates: coords, meal: swapMeal },
        notifyOnNetworkStatusChange: true
    });

    if(loading) return <Loading />
    if(error) return <Error error={error}/>

    if(!data.getMenus.length) return (
        <>
            <br/>
            <br/>
            <br/>
            <h1>😔</h1>
            <small>Por el momento no hay nadie aquí.</small>
        </>
    )
    
    return (
        <GridContainer>
            {data.getMenus.map( item => (
                <Card key={item._id} data={item} meal={meal}/>
            ))}
        </GridContainer>
    )
}

const GridContainer = styled.div`
    margin: 1.5rem auto 0;
    display: grid;
    max-width: 600px;
    grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
    column-gap: 10px;
`;

export default MenuList
