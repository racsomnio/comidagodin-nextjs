import { useState, useEffect } from 'react';
import Router from 'next/router'
import styled from 'styled-components';
import DayItem from './DayItem'
import AddPhoto from './AddPhoto';
import { string, array, object } from 'yup';
import Tooltip from '../common/Tooltip'
import { Steps } from '../../styles/common'

//Editor
import { EditorStyles } from '../../styles/EditorStyles';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';

import { useQuery, useMutation } from '@apollo/react-hooks';
import { CREATE_MENU, GET_MY_MENUS, GET_MENUS, GET_CURRENT_USER } from '../../queries';
import { swap_it } from '../../utils';

const validationSchema = object().shape({
    repeat: array().min(1, 'Selecciona al menos un día.'),
    html_menu: string().required('Tu menú no puede estar vacío.'),
});

function AddMenu() {
    const coordinates =localStorage.getItem('coords'); // use it to refetch getMenus
    
    const { data } = useQuery(GET_CURRENT_USER);
    
    const [ errors, setErrors ] = useState({});
    const [ menu, setMenu] = useState({
        repeat: [],
        html_menu: '',
        location: data.getCurrentUser.location,
        menu_image: [],
        owner: data.getCurrentUser._id
    });

    // const cs = {"blocks":[{"key":"du77p","text":"🍝Espaguetti a la boloñesa ....... $85","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":1,"length":26,"style":"BOLD"}],"entityRanges":[],"data":{}},{"key":"c183a","text":"       Con queso o sin queso","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":28,"style":"fontsize-12"}],"entityRanges":[],"data":{}},{"key":"98in7","text":"","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"dlp89","text":"(Este un ejemplo, escribe o pega tu menú aqui)","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":46,"style":"fontsize-12"}],"entityRanges":[],"data":{"text-align":"center"}}],"entityMap":{}};

    const [ contentState, setContentState ] = useState();

    function addPhoto(pic) {
        setMenu(prevState => ({
            ...prevState,
            menu_image: [ pic, ...prevState.menu_image ]
        }));
    };

    function handleChange(e) {
        const { name, value } = e.target;
        setMenu(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    function handleDaysClick(e) {
        if(e.target.className.includes(' day')){
            const dayNum = {
                Lun: 1,
                Mar: 2,
                Mie: 3,
                Jue: 4,
                Vie: 5,
                Sab: 6,
                Dom: 0
            }
            const { innerText } = e.target;

            // console.log(dayNum[innerText]);
            let arr = menu.repeat;
            const dayExists = arr.includes(dayNum[innerText]);
            
            if(!dayExists){
                arr = [...arr, dayNum[innerText]]
            }else{
                arr = arr.filter((i) => { return i !== dayNum[innerText] })
            }

            setMenu(prevState => ({
                ...prevState,
                repeat: arr
            }))
        }
    }

    function onContentStateChange(contentState) {
        
        setContentState(contentState)
        const toHTML = draftToHtml(contentState)
        setMenu(prevState => ({
            ...prevState,
            html_menu: toHTML
        }));
    }

    const handleSubmit = async (callback) => {
        const res = await callback();
        const swap_menu = Object.keys(swap_it).filter((k) => {
            if(swap_it[k] === menu.meal) return k
        });
        Router.push( `/${data.getCurrentUser.slug}`)
    }

    async function validateAllInputs (e) {
        await validationSchema.validate(
            menu,
            // { abortEarly: false }
        ).then(valid => {
            setErrors({})
            handleSubmit(createMenu);
          }).catch(err => {
            console.log('err:', err)
            setErrors(err)
        })
    }

    const [createMenu, {error, loading}] = useMutation(CREATE_MENU, {
        variables: menu,
        refetchQueries: [
            { 
                query: GET_MY_MENUS
            },
            {
                query: GET_MENUS,
                variables: {coordinates, meal: menu.meal}
            }
        ]
    });

    return (
        <Fieldset>
            <strong>Continúa Gratis aquí</strong>
            <div style={{ margin: '1rem 0 3rem'}}>
                <AddPhoto addPhotoToParentState={ addPhoto } hasImage={menu.menu_image} />
                <Message>Si subes una foto de tu menú nuestro buscador no te encontrará!</Message>
            </div>

            <Week>
                <Message>Seleccciona el día o los días que este menú está disponible</Message>
                <Tooltip 
                    error={errors.path === 'repeat' && errors.errors[0]}
                >
                    <div className="days" onClick={handleDaysClick}>
                        <DayItem day="Lun" val={1} />
                        <DayItem day="Mar" val={2} />
                        <DayItem day="Mie" val={3} />
                        <DayItem day="Jue" val={4} />
                        <DayItem day="Vie" val={5} />
                        <DayItem day="Sab" val={6} />
                        <DayItem day="Dom" val={0} />
                    </div>
                </Tooltip>
            </Week>

            <Tooltip 
                error={errors.path === 'html_menu' && errors.errors[0]}
            >
                <EditorStyles>
                    <Message>Escribe tu menú aquí abajo para que clientes te encuentren por el buscador.</Message>
                    <Editor 
                        initialContentState={contentState}
                        onContentStateChange={ onContentStateChange }
                        localization={{
                            locale: 'es',
                        }}
                        placeholder={`Escribre / Copia y pega aquí`}
                        toolbar={{
                            options: ['emoji', 'blockType', 'inline', 'history'],
                            inline: { options: ['bold', 'underline']},
                            blockType: {inDropdown: false, options: ['H2', 'Normal'], className: 'editor-h2',},
                            emoji: { 
                                emojis: [
                                    '☕','🍕','🌮','🍔','🌭', '🥟', '🌯','🥪','🥐', '🥞','🥖', '🍞',  '🍳','🍖', '🍗', '🥩', '🥓','🍣', '🥣','🍲', '🍤','🍛', '🥗','🍜', '🍝', '🍱', '🍿','🍟','🥡','🍚', '🍦', '🍩', '🍪', '🧁', '🍫', '🍬', '🍮', '🍰', '🎂', 
                                    '🌶', '🌽','🍄','🍅', '🍇', '🍉', '🍊', '🍋', '🍌', '🍍', '🍎', '🍑', '🍓', '🥭', '🍐', '🥝', '🍈', '🥑', '🥜', '🥥', '🥕', '🍠', '🥦', '🧇',
                                    '🐑','🐄','🐖','🐥','🐐', '🐠', '🦀', '🦞', '🌿', '🐛', '🦗', '🌹',
                                    '🥤', '🍽', '🍷', '🍺', '🍻', '🍹', '🍾','🍸','🥂', '🥃', '🧃',
                                    '👇', '🤘', '🖐', '👌', '👍', '👎', '🤙', '👊', '👏', '🙏','💪', '👈', '👉', '👆','💘','☎', '💵', '💳',
                                    '🌙', '🌞','🌨', '🌩', '🔥', '🧊',
                                    '🎃','⛄', '🎅','🎄','💃','👑','🎉', '🎈', '🎁','⚽', '🏀', '🏈', '⚾','🎾', '🥊', '🎳','🏆','🏁', '🎵', '🎷', '💰', '🖊', 
                                    '✅','🇲🇽','🇦🇷','🇺🇾','🇨🇴', '🇵🇪', '🇰🇷','🇯🇵','🇮🇹','🇮🇳','🇪🇦', '🇩🇪', '🇨🇺', '🇧🇷',
                                    '🤯','😀', '😁', '😂', '😉', '😋', '😎', '😍', '😴', '😌', '🤓', '😷','😭','😱','😰', '🤤','😪', '🥵', '🥶','🥴','🥳', '🥺', '🥱', 
                                    ],
                            },
                        }}
                    />
                </EditorStyles>
            </Tooltip>

            <button
                className="secondary" 
                style={{ marginTop: '2rem' }}
                onClick={validateAllInputs}
            >
                Anunciar Gratis
            </button>

            <Steps>3/3</Steps>
        </Fieldset>
    )
}

const Fieldset = styled.fieldset`
    border-radius: 10px;
    border:0;
    border-top: 4px solid #f2f2f2;
    padding: 1.5rem 0;
    background: #fff;

    @media all and (min-width: 768px){
        padding: 1.5rem 4rem 3rem;
        border: 1px solid #e6e7e8;
        min-width: 500px;
    }

    legend {
        padding-left: 1rem;
        padding-right: 1rem;
    }
`;

const Message = styled.p`
    font-size: 0.8rem;
    margin-bottom: 0.5rem;
`;

const Week = styled.div`
    text-align: center;
    margin: 0 auto 2.5rem;
    max-width: 350px;
    margin-top: 2.5rem;

    .days{
        background: #f2f2f2;
        border-radius: 10px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        height: 2rem;
        font-size: 0.8rem;
    }
`;

export default AddMenu
