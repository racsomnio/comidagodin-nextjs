import Header from './header'
import Footer from './footer'
import Headroom from 'react-headroom'
import styled from 'styled-components'

function Layout({ children, location }) {
  const isDigitaMenu = location === '/[meal]';

  return (
    <div className="container">
      <HeadContainer>
        <Headroom wrapperStyle={{ width: '100%'}} disableInlineStyles>
          <Header/>
        </Headroom>
      </HeadContainer>
      
      <main>
        {children}
      </main>
      
      <Footer/>
    </div>
  )
}
export default Layout

const HeadContainer = styled.div`
  width: 100%;
`;