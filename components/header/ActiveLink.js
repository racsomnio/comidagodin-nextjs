import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { Children } from 'react'

const ActiveLink = ({ children, activeClassName, ...props }) => {
    const { asPath } = useRouter()
    const child = Children.only(children)
    const childClassName = child.props.className || ''
    const to = props.as || props.href;

    const className =
        asPath === to
        ? `${childClassName} ${activeClassName}`.trim()
        : childClassName

    return (
        <Link {...props}>
        {React.cloneElement(child, {
            className: className || null,
        })}
        </Link>
    )
}

ActiveLink.propTypes = {
    activeClassName: PropTypes.string.isRequired,
}

export default ActiveLink