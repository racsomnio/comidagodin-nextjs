import { useState, useRef } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import styled from 'styled-components'
import UserNavItems from './UserNavItems'

function Layout({home}) {
    const [open, setOpen] = useState(false);
    const refHamburgerMenu = useRef();

    function closeHamburgerMenu() {
        setOpen(!open);
        const getWidth = refHamburgerMenu.current.clientWidth;
        document.querySelector('main').style.marginLeft = !open ? `-${getWidth}px` : 'initial';
    }

    return (
        <header>
            <Head>
                <link 
                    href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;700&display=swap" 
                    rel="preload"
                    as="style"
                    onLoad="this.onload=null;this.rel='stylesheet'"
                />
            </Head>

            <Link href="/">
                <a className="logo">
                    <img src="/nav-icons/comidagodin.svg" alt="Comida Godín" />
                    <span><img src="/nav-icons/godin.svg" alt="godin-tragon"/></span>
                </a>
            </Link>
            
            <MobileMenu className={`${open ? 'isOpen' : ''}`} onClick={ closeHamburgerMenu }>
                <span className="hamburger-menu"></span>
            </MobileMenu>

            <div 
                className={`nav-items ${open ? 'isOpen' : ''}`} 
                onClick={ closeHamburgerMenu }
                ref={refHamburgerMenu}
            >
                <div className="submenu tools">
                    <UserNavItems/>
                </div>
            </div>
            
        </header>
    )
}

const MobileMenu = styled.div`
    width: 60px;
    height: 60px;
    border-radius: 100%;
    z-index: 100;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    cursor: pointer;
    position: relative;

    &.isOpen {
        /* transform: rotate(180deg); */

        & .hamburger-menu {
            background: transparent;
            transform: rotate(180deg);

            &:before {
                transform: rotate(45deg);
                top: 0;
            }

            &:after {
                transform: rotate(-45deg);
                top: 0;
            }
        }
    }

    .hamburger-menu {
        width: 20px;
        height: 4px;
        top: 50%;
        right: 50%;
        transform: translate(50%, -3px);
        background: #7a1a7b;
        border-radius: 4px;
        position: absolute;
        transition: all 0.5s;

        &:before, &:after {
            content: "";
            width: 20px;
            height: 4px;
            position: inherit;
            background: #7a1a7b;;
            border-radius: inherit;
            transition: all 1s;
            left: 0;
        }

        &:before {
            top: -7px;
        }

        &:after {
            top: 7px;
        }
    }
`;

export default Layout
