import Link from 'next/link'
import ActiveLink from './ActiveLink'

function NavbarAuth() {
    return (
        <>
            <ActiveLink activeClassName="nav-item__selected" href="/mi/perfil">
                <a className="nav-item">
                    <img src="/icons/account.svg" alt="cv" />
                    <span>Mi CV</span>
                </a>
            </ActiveLink>
        </>
    )
}

export default NavbarAuth
