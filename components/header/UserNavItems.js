import { withApollo } from "../../utils/apollo"
import { useQuery } from "@apollo/react-hooks"
import { GET_CURRENT_USER } from '../../queries';
import NavbarUnAuth from './NavbarUnAuth'
import StoreNavbarAuth from './StoreNavbarAuth'
import UserNavbarAuth from './UserNavbarAuth'
import Signout from './Signout'

function UserNavItems() {
    const { loading, error, data, fetchMore } = useQuery(GET_CURRENT_USER);
    if(loading) return null
    if(error) return null
    
    if(!data.getCurrentUser) return <NavbarUnAuth/>;
    const {slug, full_menu, rights } = data.getCurrentUser;
    return (
        <>
            {
                data.getCurrentUser.__typename === 'Store'
                ? <StoreNavbarAuth slug={slug} full_menu={full_menu} rights={rights} />
                : <UserNavbarAuth/>
            }
            <Signout/>
        </>
    )
}

export default withApollo()(UserNavItems);
