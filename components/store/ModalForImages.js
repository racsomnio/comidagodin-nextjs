import {useState} from 'react'
import styled from 'styled-components'

function ModalForImages({ children, src }) {
    const [isOpen, toggle] = useState(false)

    function toggleOpen() {
        toggle(!isOpen);
    }

    return (
        <ModalContainer onClick={toggleOpen}>
            {children}
            <Modal isOpen={isOpen} onClick={toggleOpen}>>
                <img src={src} alt="modal"/>
            </Modal>
        </ModalContainer>
    )
}
export default ModalForImages

const ModalContainer = styled.div`
    cursor: pointer;
`;

const Modal = styled.div`
    position: fixed;
    width: 100%; 
    opacity: ${ ({isOpen}) => isOpen ? '1' : '0' };
    height: ${ ({isOpen}) => isOpen ? '100%' : '0' };
    left: 0;
    bottom: 0;
    z-index: 4;
    background: rgba(0,0,0,0.9);
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    transition: height 0.3s;

    img {
        width: 90%;
        max-width: 800px;
        border-radius: 10px;
        box-shadow: 0 0 20px -10px #000;
    }
`;