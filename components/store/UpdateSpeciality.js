import { useState } from 'react'
import AutocompleteV2 from '../common/AutocompleteV2'
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_SPECIALITY, GET_MY_STORE } from '../../queries';
import { specialities } from '../../utils'

const UpdateFoodType = ({ speciality }) => {
    const [ specialityArray, setSpecialityArray ] = useState([speciality[0]])

    const [ updateStoreFoodType, {loading} ] = useMutation(UPDATE_STORE_SPECIALITY, {
        variables: { speciality: specialityArray },
        refetchQueries: [{query: GET_MY_STORE}],
    })

    async function handleOnSelect(selected) {
        await setSpecialityArray([ selected ]);

        await updateStoreFoodType();
    }

    return (
        <span>
            <AutocompleteV2 
                suggestions={specialities} 
                placeholder="Ej: Sushi, Birria, ..." 
                defaultValue={specialityArray[0]}
                onSelect={handleOnSelect}
            />
        </span>
    )
}

export default UpdateFoodType
