import Link from 'next/link'
import styled from 'styled-components';
import { Icon } from '../../styles/common'

const GetStoresCard = ({ data }) => {
    const convertToMins = (m) => {
        return Math.ceil((m * 1.3) / 60);
    }

    return (
        <Item>
            <Link href={`/${data.slug}`}>
                <a>
                    <CardContent>
                        <Img>
                            
                            {/* <img 
                                src={ 
                                    (data.menu_image.length) 
                                    ? `${data.menu_image[0].secure_url}` 
                                    : `${process.env.PUBLIC_URL}/no-image.png`
                                } 
                                alt="{data.name}"
                            /> */}
                            <span className="store-img" style={{
                                backgroundImage: `url(${ data.profile_img.length && data.profile_img[0].secure_url})`
                            }}></span>
                        </Img>
                        <div className="second-col">
                            <h2 className="card-title" >{data.name}</h2>
                            <div className="business">
                                    {
                                        data.typeOfBusiness.map( type => <span key={type} className="business-type">{type}</span>)
                                    }
                            </div>
                            <div className="food-type">
                                <span>Comida {data.food_type.type}</span>
                            </div>
        
                            <span className="card-dis">
                            {
                                data.delivery && <Icon size={'30px'}><img src ="/icons/delivery.svg" alt="entrega"/></Icon>
                            }
                            {
                                data.takeaway && <Icon size={'30px'}><img src ="/icons/takeaway.svg" alt="llevar"/></Icon>
                            }
                            {
                                data.only_cash && <Icon size={'30px'}><img src ="/icons/cash.svg" alt="cash"/></Icon>
                            }
                            {
                                data.card && <Icon size={'30px'}><img src ="/icons/card.svg" alt="cash"/></Icon>
                            }
                            {
                                (data.vegan_option || data.vegetarian_option) && <Icon size={'30px'}><img src ="/icons/leaf.svg" alt="cash"/></Icon>
                            }
                            {
                                data.has_breakfast && <Icon size={'30px'}><img src ="/icons/breakfast.svg" alt="cash"/></Icon>
                            }
                            {
                                data.has_lunch_special && <Icon size={'30px'}><img src ="/icons/lunch-special.svg" alt="cash"/></Icon>
                            }
                            {
                                data.wa_orders && <Icon size={'30px'}><img src ="/icons/whatsapp.svg" alt="cash"/></Icon>
                            }
                            </span>
                            <div className="card-dis">Aprox. <strong>{convertToMins(data.distance)}</strong> mins. caminando</div>
                        </div>
                    </CardContent>
                </a>
            </Link>
        </Item>
    )
}

export default GetStoresCard;

const Item = styled.div`
    position: relative;
    margin-bottom: 2rem;
`;

const CardContent = styled.div`
    display: flex;
    justify-content: flex-start;
    padding: 0 1rem;

    .second-col {
        padding-left: 0.5rem;
        text-align: left;
    }

    .card-title {
        text-transform: capitalize;
        /* font-family: 'Baloo 2',cursive; */
        position: relative;
        font-size: 2rem;
        margin-top: 0;
        margin-bottom: 0;
    }
    .store-img {
        width: 80px;
        height: 80px;
        background: #f2f2f2;
        display: block;
        margin: auto;
        border-radius: 50%;
        box-shadow: -1px 0 #f2f2f2;
        background-size: cover;
    }

    .card-dis {
        vertical-align: middle;
        margin-top: 0.5rem;
        margin-bottom: 2rem;
    }

    .food-type {
        margin-bottom: 0.8rem;
        margin-top: 0.8rem;
        span {
            background: #fbdb6d;
            border-radius: 30px;
            padding: 0.4rem 1rem;
            -webkit-box-decoration-break: clone;
            -o-box-decoration-break: clone;
            box-decoration-break: clone;
            line-height: 1.5rem;
        }
    }
    .business {
        margin: 0.3rem;
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
    }
    .business-type {
        text-transform: capitalize;
        padding-right: 0.5rem;
    }

    ${Icon} {
        margin-bottom: 0.5rem;
    }
`;

const Img = styled.div`
    position: relative;
    padding-top: 0.3rem;

    img {
        border-radius: 0;

        @media all and (min-width: 410px) {
            border-radius: 10px;
        }
    }
`;
