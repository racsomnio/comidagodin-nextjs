import { useState, useRef, useEffect } from 'react'
import { useQuery } from "@apollo/react-hooks"
import Head from 'next/head'
import dynamic from 'next/dynamic'
import styled, { css, keyframes } from 'styled-components'
import Headroom from 'react-headroom'
import smoothscroll from 'smoothscroll-polyfill';
import { GET_STORE_BY_SLUG } from '../../queries'
import Loading from '../common/Loading'
import Error from '../common/Error'
import { Icon, Divider } from '../../styles/common';
import ShowLocation from '../common/ShowLocation'
import { daysLong, countryToFlag } from '../../utils';
import FullMenuCard from './FullMenuCard'


const ShareButtons = dynamic(
    () => import('../menu/ShareButtons'),
    { ssr: false }
)

const FullInfo = ({ slug }) => {
    const { loading, error, data } = useQuery(GET_STORE_BY_SLUG, {
        variables: { slug },
    });
    const [ showSections, setShowSections ] = useState(false)
    const sectionRefs= useRef({});
    const sectionsWrapperRef = useRef();

    useEffect(() => {
        smoothscroll.polyfill();
    }, [])

    // const todayIs = daysLong[new Date().getDay()];

    if(loading) return <Loading />
    if(error) return <Error error={error}/>

    const { name, profile_img, cover_img, typeOfBusiness, location, food_type, phone,
        delivery, takeaway, only_cash,card, reservations, has_breakfast, has_lunch_special, parking, valet_parking,
        wifi, open_tv, cable, ppv, speak_english, english_menu, vegan_option, vegetarian_option, no_gluten_option,
        no_shellfish_option, live_music, dancing, has_karaoke, rooftop, terrace, garden, smoking_area, kids_menu,
        kids_area, private_room, pet_friendly, catering, gay, has_healthy_food, has_alcohol, has_billiard,
        has_foosball, has_pool, big_groups, hours, speciality, full_menu } = data.getStoreBySlug;

    const hasImage = cover_img.length 
        ? cover_img[0].secure_url
        : ''; 
    
    const active_full_menu = full_menu.menu.filter(section => section.active === null || section.active)
    
    const scrollToRef = (ref) => {
        window.scroll({ top: sectionRefs.current[ref].offsetTop - 90, left: 0, behavior: 'smooth' })
    };

    return (
        <>
            <Head>
                <title>{name}</title>
                <meta property="og:title" content={name} />
                <meta property="og:image" content={hasImage} />
                <meta property="og:type" content="website" />
                <meta name="description" property="og:description" content={`Te invitamos a ver nuestro menú`} />
                <meta property="og:locale" content="es_MX" />
                <meta property='twitter:title' content={name} />
                <meta property='twitter:image' content={hasImage}/>
                <meta name="twitter:card" content="summary_large_image"/>
                
            </Head>

            <FullMenuGrid>
            <Col>
                    <StoreInfo>
                        {
                            profile_img.length ?
                            <img 
                                src={profile_img[0].secure_url} 
                                style={{
                                    borderRadius: '10px',
                                    margin: 'auto',
                                    width: '150px',
                                    marginTop: '-60px',
                                    borderTop: '1px solid #e6e7e8',
                                    background: '#fff',
                                }}
                            />
                            : null
                        }
                        
                        <h2 style={{ margin: '1rem auto 0.3rem', fontSize: '2rem'}}>
                            {name}
                        </h2>
                        <BusinessType>
                            {
                                typeOfBusiness.map(type => <span key={type} className="business-type">&nbsp;{type.replace(/-/g, ' ')}&nbsp;</span>)
                            }
                        </BusinessType>
        
                        <div style={{ textAlign: 'center', padding: '0 1rem' }}>
                            {   food_type.type &&
                                <div style={{ marginBottom: '1rem' }}>
                                    <span>Comida {food_type.type}</span>
                                    <span style={{ fontSize: '1.2rem', verticalAlign: 'middle'}}>&nbsp;{countryToFlag(food_type.code)}</span>
                                </div>
                            }
                            <div style={{ marginBottom: '1.5rem' }}>
                                <ShowLocation location={location}>
                                    {location.address}
                                </ShowLocation>
                            </div>
                            
                            <Phone style={{ marginBottom: '1rem' }}>
                                <Icon size={'20px'}><img src="/icons/phone.svg" alt="tel" /></Icon>
                                <a href={`tel:+52${phone[0].number}`}>&nbsp;Llama al <strong>{phone[0].number}</strong></a>
  
                            </Phone>
                            <div style={{ textAlign: 'left', marginBottom: '1.5rem', borderBottom: '1px solid #e6e7e8', paddingBottom: '0.5rem' }}>
                                {
                                    phone[0].whatsapp && (
                                        <>
                                            <Icon size={'20px'}><img src="/icons/whatsapp.svg" alt="whatsapp" /></Icon>
                                            <a 
                                                href={`https://wa.me/52${phone[0].number}?text=Hola%2C%20v%C3%AD%20tu%20men%C3%BA%20en%20Comida%20God%C3%ADn%2C%20`}
                                                target="_blank"
                                            >&nbsp;Envía un <strong>Whatsapp</strong></a>
                                        </>
                                    )
                                }
                            </div>
                        </div>
                        
                        <div>
                            {
                                hours.map( hour => <small key={hour._id}>
                                        <strong>{`${hour.from} a ${hour.to}`}</strong>
                                        <span>{` de ${hour.open_hr}:${hour.open_min} ${hour.open_ampm} a ${hour.close_hr}:${hour.close_min} ${hour.close_ampm}`}</span>
                                        <br/>
                                    </small>
                                )
                            }
                        </div>
                    </StoreInfo>
                </Col>
                
                {
                    full_menu && active_full_menu.length > 1 &&
                    <HeadroomWrapper showSections={showSections} ref={sectionsWrapperRef}>
                        <Headroom 
                            disableInlineStyles 
                            wrapperStyle={{ paddingBottom: '2rem', paddingTop: '1rem'}}
                            pinStart={sectionsWrapperRef.current && (sectionsWrapperRef.current.offsetHeight + sectionsWrapperRef.current.scrollHeight) || 400}
                            onPin={() => setShowSections(true)}
                            onUnfix={() => setShowSections(false)}
                        >
                            <SectionMenu>
                            {
                                active_full_menu.map( (menu, i) =>  menu.section_title &&
                                <span 
                                    key={`sectionName${i}`} 
                                    onClick={() => scrollToRef(`secRef${i}`)}
                                    className="section-menu-item"
                                >
                                    {menu.section_title}
                                </span>
                                )
                            }
                            </SectionMenu>
                        </Headroom>
                    </HeadroomWrapper>
                }

                <Col>
                    {
                        full_menu && active_full_menu.map( (menu, i) => <div
                            key={`fullStoreInfo${i}`} 
                            ref={(el) => {
                                sectionRefs.current[`secRef${i}`] = el
                            }}
                        >
                            <FullMenuCard 
                                menuSection={menu}
                                addItem={ addItem }
                                wa_orders ={wa_orders}
                                toggleOrderItem={toggleOrderItem}
                                handleItemSelected={handleItemSelected}
                            />
                        </div>
                        )
                    }
                </Col>

                {/* <Options>
                    {
                        delivery &&
                        <span>
                            <Icon size="25px"><img src="/icons/delivery.svg"/></Icon>
                            <span>Entrega a domicilio</span>
                        </span>
                    }
                    {
                        takeaway &&
                        <span>
                            <Icon size="25px"><img src="/icons/takeaway.svg"/></Icon>
                            <span>Para llevar</span>
                        </span>
                        
                    }
                
                    {
                        only_cash &&
                        <span>
                            <Icon size="25px"><img src="/icons/cash.svg"/></Icon>
                            <span>Efectivo</span>
                        </span>
                        
                    }
                
                    {
                        card &&
                        <span>
                            <Icon size="25px"><img src="/icons/card.svg"/></Icon>
                            <span>Se aceptan tarjetas</span>
                        </span>
                        
                    }
                    {
                        reservations &&
                        <span>
                            <Icon size="25px"><img src="/icons/reservations.svg"/></Icon>
                            <span>Reservaciones</span>
                        </span>
                        
                    }
                    {
                        has_breakfast &&
                        <span>
                            <Icon size="25px"><img src="/icons/breakfast.svg"/></Icon>
                            <span>Desayunos</span>
                        </span>
                        
                    }
                    {
                        has_lunch_special &&
                        <span>
                            <Icon size="25px"><img src="/icons/lunch-special.svg"/></Icon>
                            <span>Menú del día</span>
                        </span>
                        
                    }
                    {
                        parking &&
                        <span>
                            <Icon size="25px"><img src="/icons/parking.svg"/></Icon>
                            <span>Estacionamiento</span>
                        </span>
                        
                    }
                    {
                        valet_parking &&
                        <span>
                            <Icon size="25px"><img src="/icons/valet-parking.svg"/></Icon>
                            <span>Valet parking</span>
                        </span>
                        
                    }  
                    {
                        wifi &&
                        <span>
                            <Icon size="25px"><img src="/icons/wifi.svg"/></Icon>
                            <span>Wifi</span>
                        </span>
                        
                    }                          
                    {
                        open_tv &&
                        <span>
                            <Icon size="25px"><img src="/icons/tv.svg"/></Icon>
                            <span>TV abierta</span>
                        </span>
                        
                    }
                
                    {
                        cable &&
                        <span>
                            <Icon size="25px"><img src="/icons/tv.svg"/></Icon>
                            <span>Cable</span>
                        </span>
                        
                    }
                
                    {
                        ppv &&
                        <span>
                            <Icon size="25px"><img src="/icons/ppv.svg"/></Icon>
                            <span>Pago Por Evento</span>
                        </span>
                        
                    }
                
                    {
                        speak_english &&
                        <span>
                            <Icon size="25px"><img src="/icons/speaking.svg"/></Icon>
                            <span>We Speak English</span>
                        </span>
                        
                    }
                
                    {
                        english_menu &&
                        <span>
                            <Icon size="25px"><img src="/icons/menu.svg"/></Icon>
                            <span>English Menu</span>
                        </span>
                        
                    }
                
                    {
                        vegan_option &&
                        <span>
                            <Icon size="25px"><img src="/icons/vegan.svg"/></Icon>
                            <span>Opción Vegana</span>
                        </span>
                        
                    }
                    {
                        vegetarian_option &&
                        <span>
                            <Icon size="25px"><img src="/icons/vegetarian.svg"/></Icon>
                            <span>Opción Vegetariana</span>
                        </span>
                        
                    }
                    {
                        no_gluten_option &&
                        <span>
                            <Icon size="25px"><img src="/icons/gluten-free.svg"/></Icon>
                            <span>Sin Gluten</span>
                        </span>
                        
                    }
                
                    {
                        no_shellfish_option &&
                        <span>
                            <Icon size="25px"><img src="/icons/shrimp.svg"/></Icon>
                            <span>Opción sin mariscos</span>
                        </span>
                        
                    }
                    {
                        live_music &&
                        <span>
                            <Icon size="25px"><img src="/icons/live-music.svg"/></Icon>
                            <span>Música en vivo</span>
                        </span>
                        
                    }
                    {
                        dancing &&
                        <span>
                            <Icon size="25px"><img src="/icons/dancing.svg"/></Icon>
                            <span>Se puede bailar</span>
                        </span>
                        
                    }
                    {
                        has_karaoke &&
                        <span>
                            <Icon size="25px"><img src="/icons/karaoke.svg"/></Icon>
                            <span>Karaoke</span>
                        </span>
                        
                    }                            
                    {
                        rooftop &&
                        <span>
                            <Icon size="25px"><img src="/icons/rooftop.svg"/></Icon>
                            <span>Rooftop ó Skybar</span>
                        </span>
                        
                    }
                    {
                        terrace &&
                        <span>
                            <Icon size="25px"><img src="/icons/terrace.svg"/></Icon>
                            <span>Terraza</span>
                        </span>
                        
                    }
                    {
                        garden &&
                        <span>
                            <Icon size="25px"><img src="/icons/garden.svg"/></Icon>
                            <span>Jardín</span>
                        </span>
                        
                    }
                    {
                        smoking_area &&
                        <span>
                            <Icon size="25px"><img src="/icons/smoking-area.svg"/></Icon>
                            <span>Área para fumar</span>
                        </span>
                        
                    }
                    {
                        kids_menu &&
                        <span>
                            <Icon size="25px"><img src="/icons/kids-menu.svg"/></Icon>
                            <span>Menú para niños</span>
                        </span>
                        
                    }
                    {
                        kids_area &&
                        <span>
                            <Icon size="25px"><img src="/icons/kids-area.svg"/></Icon>
                            <span>Área para niños</span>
                        </span>
                        
                    }
                    {
                        private_room &&
                        <span>
                            <Icon size="25px"><img src="/icons/private-room.svg"/></Icon>
                            <span>Área privada</span>
                        </span>
                        
                    }
                    {
                        pet_friendly &&
                        <span>
                            <Icon size="25px"><img src="/icons/pet-friendly.svg"/></Icon>
                            <span>Pet friendly</span>
                        </span>
                        
                    }
                    {
                        catering &&
                        <span>
                            <Icon size="25px"><img src="/icons/catering.svg"/></Icon>
                            <span>Catering</span>
                        </span>
                        
                    }
                    {
                        gay &&
                        <span>
                            <Icon size="25px"><img src="/icons/gay.svg"/></Icon>
                            <span>LGBTQ</span>
                        </span>
                        
                    }
                    {
                        has_healthy_food &&
                        <span>
                            <Icon size="25px"><img src="/icons/healthy-food.svg"/></Icon>
                            <span>Saludable</span>
                        </span>
                        
                    }
                    {
                        has_alcohol &&
                        <span>
                            <Icon size="25px"><img src="/icons/alcohol.svg"/></Icon>
                            <span>Bebidas alcohólicas</span>
                        </span>
                        
                    }
                    {
                        has_billiard &&
                        <span>
                            <Icon size="25px"><img src="/icons/billiard.svg"/></Icon>
                            <span>Billar</span>
                        </span>
                        
                    }
                    {
                        has_foosball &&
                        <span>
                            <Icon size="25px"><img src="/icons/foosball.svg"/></Icon>
                            <span>Futbolito</span>
                        </span>
                        
                    }
                    {
                        has_pool &&
                        <span>
                            <Icon size="25px"><img src="/icons/pool.svg"/></Icon>
                            <span>Piscina</span>
                        </span>
                        
                    }
                    {
                        big_groups &&
                        <span>
                            <Icon size="25px"><img src="/icons/big-groups.svg"/></Icon>
                            <span>Maneja grupos grandes</span>
                        </span>
                        
                    }
                </Options> */}
            </FullMenuGrid>

            <ShareButtons 
                url={`/${slug}`} 
                text={name.toUpperCase()} 
                media={hasImage}
            />
        </>
    )
}

export default FullInfo

const FullMenuGrid = styled.div`
    display: flex;
    justify-content: left;
    flex-wrap: wrap;
    justify-content: center;
`;

const Phone = styled.div`
    margin-bottom: 1rem;
    background: #fbdb6d;
    margin-left: -3rem;
    margin-right: -3rem;
    padding: 0.6rem 1rem 0.6rem 3rem;
    border-radius:10px 10px 2px 2px;
    text-align: left;
    position: relative;
    border: 1px solid #e6e7e8;
    box-shadow: 0 6px 9px -3px rgba(0,0,0,0.2);

    &:before, &:after {
        content: "";
        position: absolute;
        z-index: -1;
        width: 1rem;
        height: 1rem;
        background: #e8cd6c;
        border-radius: 0 0 0 0.5rem;
        left: 0;
        bottom: -0.4rem;
    }
    &:after {
        left: initial;
        right: 0;
        border-radius: 0 0 0.5rem 0;
    }
`;

const Col = styled.div`
    flex: 0 1 100%;
    text-align: left;
    padding: 0 1rem;

    @media all and (min-width: 600px){
        flex: 0 1 400px;
    }
`;

const StoreInfo = styled.div`
    text-align: center;
    position: relative;
    padding: 1.5rem 1.5rem 2rem;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    background: #fff;
    margin-bottom: 2.5rem;
    margin-top: 20px;
`;

const Options = styled.div`
    position: relative;
    font-size: 0.7rem;
    padding: 0 1rem;
    display: flex;
    flex-wrap: wrap;
    max-width: 800px;
    justify-content: space-between;

    &:before {
        content: "Servicios";
        width: 100%;
        display: block;
        text-align: center;
    }

    & > span {
        padding: 1rem;
    }
`;

const BusinessType = styled.div`
    margin-bottom: 1rem;
    text-transform: capitalize;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
`;

const HeadroomWrapper = styled.div`
    .headroom-wrapper {
        height: initial !important;
    }
    .headroom {
        background: #fff;
        border-bottom: 1px solid #e6e7e8;
        box-shadow: 0 0 10px 5px rgba(0,0,0,0.2);
        transition: none;
    }
    .headroom--unfixed {
        position: relative;
        transform: translateY(0%);
        background: none;
        border-bottom: 0;
        box-shadow: none;
    }
    .headroom--scrolled {
        /* transition: transform 200ms ease-in-out; */
    }
    .headroom--unpinned {
        position: fixed;
        transform: translateY(0%);
        padding-left: 0.5rem;
        padding-right: 0.5rem;
        animation: ${ ({showSections}) => !showSections ? css`${slideDown} 0.5s linear` : 'none'};
    }
    .headroom--pinned {
        position: fixed;
        transform: translateY(0%);
        padding-left: 0.5rem;
        padding-right: 0.5rem;
    }
`;

const SectionMenu =  styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    font-size: 0.8rem;

    .headroom--unpinned &, .headroom--pinned & {
        flex-wrap: nowrap;
        justify-content: flex-start;
        overflow: scroll;

        .section-menu-item {
            box-shadow: none;
        }
    }

    .section-menu-item {
        padding: 0.5rem 1rem;
        margin: 0.5rem 0.2rem;
        border-radius: 30px;
        box-shadow: 0 0 0 1px rgba(0,0,0,0.1);
        background: #fff;
        display: flex;
        align-items: center;
    }
`;

const slideDown = keyframes`
  from {
    transform: translateY(-100%);
  }

  to {
    transform: translateY(0%);
  }
`;