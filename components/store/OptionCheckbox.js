import { useState } from 'react'
import styled from 'styled-components';
import { useMutation } from '@apollo/react-hooks';
import { UPDATE_STORE_BOOLEANS, GET_MY_STORE } from '../../queries';
import { Icon, Checkbox } from '../../styles/common';

const CheckboxItem = ({ flag, res, icon, title }) => {
    const [isChecked, setIsChecked] = useState(res)
    const handleChange = async (callback) => {
        await setIsChecked(!isChecked);
        await callback({
            variables: { flag, res: !isChecked }
        });
    }

    const [updateStoreBooleans] = useMutation(UPDATE_STORE_BOOLEANS, {
        refetchQueries: [{query: GET_MY_STORE}],
        awaitRefetchQueries: true,
    })

    return (
        <CheckboxParent>
            <Icon><img src={`/icons/${icon}`} alt="icono" /></Icon>
            <span>{title}</span>

            <Checkbox className="switch">
                <input 
                    type="checkbox" 
                    checked={isChecked} 
                    onChange={() => handleChange(updateStoreBooleans)} 
                />
                <span className="slider"></span>
            </Checkbox>
        </CheckboxParent>
    )
}

const CheckboxParent = styled.div`
    text-align: left;
    display: block;
    width: 100%;
    max-width: 260px;
    margin: auto;
    padding-bottom: 1rem;
`;

export default CheckboxItem
