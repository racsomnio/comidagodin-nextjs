import { useRef, useState, useEffect } from 'react'
import styled from 'styled-components'
import ModalForImages from './ModalForImages'
import Modal from '../common/Modal'

function PreviewAccordion({menuSection, wa_orders, addItem, toggleOrderItem, handleItemSelected}) {
    const [active, setActive] = useState("active");
    const [height, setHeight] = useState("auto");
    const [ showModal, setShowModal ] = useState(false)
    const [ item, setItem ] = useState({})
    const contentRef = useRef(null);
    const imageRef = useRef(null);

    useEffect( () => {
        imageRef.current && imageRef.current.addEventListener('load', handleImgHeight)

        return () => imageRef.current && imageRef.current.removeEventListener('load', handleImgHeight)
    }, [])

    function handleImgHeight(){
        const calcHeight = imageRef.current ? contentRef.current.scrollHeight + imageRef.current.clientHeight : contentRef.current.scrollHeight;
        setHeight(`${calcHeight}px`)
    }

    function toggleAccordion() {
        setHeight(
            active === "active" ? "0" : `${contentRef.current.scrollHeight}px`
        );
        setActive(active === "" ? "active" : "");
    }

    function closeModal() {
        setShowModal(false)
    }

    function handleAddQty(qty) {
        setItem(prevState => ({
            ...prevState,
            qty
        }))
    }

    function handleReqs(e) {
        const { value } = e.target;
        setItem(prevState => ({
            ...prevState,
            special_request: value,
        }))
    }

    function handleClickPriceWhenMany(price) {
        setItem(prevState => ({
            ...prevState,
            price: [price]
        }))
    }

    function updateOrder(){
        addItem(item);
        closeModal();
    }
    return (
        <PreviewSection active={active}>
            <div className="preview-section">
                {
                    menuSection.section_title && 
                    <div 
                        className="preview-header"
                        onClick={toggleAccordion}
                    >
                        {menuSection.section_title}
                    </div>
                }
                
                <div 
                    className="accordion"
                    ref={contentRef}
                    style={{ maxHeight: `${height}` }}
                >   
                    {
                        menuSection.section_description && 
                        <div className="preview-desc mb">
                            {menuSection.section_description}
                        </div>
                    }
                    <div className="preview-food">
                        {menuSection.food_items.filter( food => food.active === null || food.active).map( (f,i) => (
                            <div 
                                key={`f.food_name${i}`} 
                                className="preview-food-wrap"
                            >
                                <div 
                                    className="preview-food-info"
                                    onClick={() => {
                                        if(wa_orders) {
                                            handleItemSelected({
                                                food_name: f.food_name,
                                                qty: 1,
                                                special_request: '',
                                                price: f.food_price,
                                                priceSelected: 0,
                                            })
                                            toggleOrderItem();
                                        }
                                    }}
                                >
                                    <div className="preview-food-name">
                                        {f.food_name}
                                    </div>
                                    <div className="preview-food-desc">
                                        {f.food_description}
                                    </div>
                                    {
                                        wa_orders &&
                                        <span className="pedir">Pedir</span>
                                    }
                                    <div className="preview-prices">
                                        {
                                            // (f.food_image || f.food_price.length > 1 ) &&
                                            f.food_price.map((p,j) => (
                                                <div 
                                                    key={`pricekey${i}${j}`} 
                                                    className="preview-prices-item left"
                                                >
                                                    <strong className="preview-prices-value">
                                                        {f.food_price[j].price_value && '$'}{f.food_price[j].price_value}
                                                    </strong>
                                                    <span className="preview-prices-desc">
                                                        &nbsp;{f.food_price[j].price_description}
                                                    </span>
                                                </div> 
                                            ))
                                        }
                                    </div>
                                    <div className="preview-tag-wrap">
                                        {
                                            f.food_tags.map( tag => (
                                                <span key={`preview${tag}`}className="preview-tag">
                                                    <span className={`tag-icon ${tag.toLowerCase()}`}></span>
                                                    <span className="tag-name"> {tag}</span>
                                                </span>
                                            ))
                                        }
                                    </div>
                                </div>
                                { f.food_image && 
                                    <ModalForImages src={f.food_image}>
                                        <Pic src={f.food_image}/>
                                    </ModalForImages> 
                                    // :
                                    
                                    // f.food_price.length === 1
                                    // &&  
                                    // <div>
                                    //     {
                                    //     f.food_price.map((p,j) => (
                                    //         <div key={`pricekey${i}${j}`} className="preview-prices-item">
                                    //             <span className="preview-prices-value">
                                    //                 {f.food_price[j].price_value && '$'}{f.food_price[j].price_value}
                                    //             </span>
                                    //             <span className="preview-prices-desc">
                                    //                 &nbsp;{f.food_price[j].price_description}
                                    //             </span>
                                    //         </div> 
                                    //     ))
                                    //     }
                                    // </div>
                                }
                                
                            </div>
                        ))}
                    </div>

                    {
                        menuSection.section_footer &&
                        <div className="preview-desc mt">
                            {menuSection.section_footer}
                        </div>
                    }
                    
                    {
                    menuSection.section_image &&
                        <div className="preview-section-img">
                            <ModalForImages src={menuSection.section_image}>
                                <img 
                                    ref={imageRef}
                                    src={menuSection.section_image}  
                                    alt={menuSection.section_title} 
                                />
                            </ModalForImages>
                            
                        </div>
                    }
                </div>
                <div className="dots" onClick={toggleAccordion}>...</div>
            </div>
        </PreviewSection>
    )
}

export default PreviewAccordion


const PreviewSection = styled.div`
    background: #fff;
    border-radius: 10px;
    margin-bottom: 3.5rem;
    box-shadow: 0 0 0 1px rgba(0,0,0,0.1);
    position: relative;
    text-align: center;
    margin-top: 2rem;
    position: relative;

    .preview-section {
        border-radius: inherit;
    }

    .dots {
        font-size: 3rem;
        bottom: 0;
        left: 0;
        border-radius: inherit;
        width: 100%;
        position: relative;
        background: #fff;
        overflow: hidden;
        height: ${ ({ active }) => active ? '0' : '60px'};
        transition: height 0.2s;
    }

        .preview-header {
            background: #7a1a7b;
            color: #fff;
            border-radius: 30px;
            font-weight: bolder;
            padding: 0.8rem 1.5rem;
            display: inline-block;
            transition: all 0.5s;
            margin-top: -1.4rem;
            vertical-align: middle;
            max-width: 90%;
            cursor: pointer;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
        }

        .preview-desc {
            padding: 1rem;
            white-space: pre-wrap;
            
            &.mt {
                margin-top: 1rem;
            }
            &.max-block-size {
                margin-bottom: 1rem;
            }
        }

        .preview-food-desc {
            padding: 0.3rem 0;
            white-space: pre-wrap;
        }

        .preview-section-img {
            padding: 1rem;
            
            img {
                border-radius: 10px;
            }
        }

        .preview-food-wrap {
            display: flex;
            padding: 2rem 1rem;
            justify-content: space-between;
            text-align: left;
            position: relative;
            flex-direction: row-reverse;

            &:after{
                content:"";
                width: 80%;
                border-bottom: 1px dashed rgba(0,0,0,0.2); 
                position: absolute;
                bottom: 0;
                left: 50%;
                transform: translateX(-50%);
                cursor: pointer;
                font-weight: bold;
            }

            .pedir {
                background: #7a1a7b;
                border-radius: 30px;
                padding: 0.5rem;
                color: #fff;
                width: 65px;
                text-align: center;
                font-weight: bold;
                float: right;
                margin: 0.5rem;
            }

            .preview-food-info {
                flex: 3;
                padding-left: 1rem;
            }
        }

        .preview-prices-value {
            font-size: 1.2rem;
        }

        .preview-tag-wrap {
            margin-top: 0.5rem;
            display: flex;
            flex-wrap: wrap;
            justify-content: flex-start;

            .preview-tag {
                font-size: 0.75rem;
                padding: 0.5rem 0;
                margin-right: 1rem;
                background: rgba(255,255,255, 0.3)
            }

            .tag-icon {
                vertical-align: middle;
                padding: 7px 12px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: 22px;
                margin-right: 3px;

                &.popular {
                    background-image: url('/icons/recommended.svg');
                }
                &.picoso {
                    background-image: url('/icons/spicy.svg');
                }
                &.vegetariano {
                    background-image: url('/icons/leaf.svg');
                }
                &.saludable {
                    background-image: url('/icons/healthy-food.svg');
                }
            }

            .tag-name {
                vertical-align: middle;
            }
        }

        .preview-prices {
            display: flex;
            /* text-align: center; */
            flex-wrap: wrap;
            align-items: flex-end;
            justify-content: flex-start;

            .preview-prices-item {
                padding-right: 1rem;
                
                &.left {
                    padding-top: 0.5rem;
                }
            }

            .preview-prices-desc {
                font-size: 0.85rem;
            }
        }

        .preview-food-name {
            font-weight: bolder;
            margin-bottom: 0.2rem;
            font-size: 1.5rem;
        }

    .accordion {
        overflow: hidden;
        opacity: ${ ({ active }) => active ? '1' : '0'};
        transition: all 0.2s;
    }
`;


const Pic = styled.div`
    background-image: ${ ({src}) => src ? `url(${src})`: 'none'};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    border-radius: 40px;
    width: 100%;
    height: 65px;
    min-width: 60px;
`;