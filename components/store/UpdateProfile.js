import styled from 'styled-components'
import { Icon, H1 } from '../../styles/common';
import UploadProfilePhoto from './UploadProfilePhoto'
import UpdatePhones from './UpdatePhones'
import UpdateOptions from './UpdateOptions'
import UpdateFoodType from './UpdateFoodType'
import UpdatePopularDish from './UpdatePopularDish'
import UpdateHours from './UpdateHours'

function UpdateProfile({ data }) {
    const { name, email, typeOfBusiness, location, cover_img, profile_img, phone, food_type, speciality, hours } = data;

    return (
        <UpdateProfileWrap>
            {/* <Banner>
                <UploadProfilePhoto img={cover_img} field="cover_img" /> */}
            {/* </Banner> */}
            <div>
                <h1>{name}</h1>
                <div style={{ marginBottom: '0.5rem' }}>Sube tu logotipo</div>
                <CardContent>
                    <UploadProfilePhoto img={profile_img} field="profile_img" />
                </CardContent>
            </div>
            <GridContainer>
                <ProfileCard>
                    <div className="borderBottom"><small><strong>Email:</strong></small> {email}</div>

                    <div>
                        <Icon ><img src='/icons/pin.svg' alt="mapa" /></Icon>
                        <span>{location.address}</span>
                    </div>
    
                    <div className="capitalize borderTop"><small><strong>Establecimiento:</strong></small> {typeOfBusiness}</div>
                </ProfileCard>
                

                <ProfileCard>
                    <UpdatePhones phone={phone} />
                </ProfileCard>
                
                <ProfileCard>
                    <div><small style={{ margin: '0 0 0.5rem', display: 'block' }}><strong>Tipo de Comida</strong></small>
                    <UpdateFoodType food_type={food_type} />
                    </div>
                </ProfileCard>

                <ProfileCard>
                    <div style={{ margin: '0 0 0.5rem' }}><small><strong>Platillo más popular</strong></small></div>
                    <UpdatePopularDish speciality={speciality} />
                </ProfileCard>
            </GridContainer>
            <ProfileCard style={{ marginBottom: '1rem' }}>
                <UpdateHours storeHours={hours} />
            </ProfileCard>

            <ProfileCard>
                <UpdateOptions data={data} />
            </ProfileCard>
        </UpdateProfileWrap>
    )
}

const UpdateProfileWrap = styled.div`
    max-width: 800px;
    padding: 0 1rem;
    margin: 0 auto 2rem;
    
    h3 {
        text-transform: capitalize;
        margin: 0;
    }
`;

const CardContent = styled.div`
    /* position: absolute; */
    /* top: 100%; */
    text-align: right;
    text-align: center;
    width: 100%;
`;

const Banner = styled.div`
  
  height: 60vh;
  max-width: 960px;
  margin: auto;
  position: relative;

  & + * {
    margin: 85px 0 1rem;
  }
`;

const GridContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
    max-width: 800px;
    margin: 1rem auto 1rem;
    align-items: center;
    /* grid-auto-rows: 120px; */
    grid-gap: 1rem;
`;

const ProfileCard = styled.div`
    background: #fff;
    border-radius: 10px;
    border: 1px solid #e6e7e8;
    padding: 1rem 1rem 1.5rem;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;

    & > div {
        margin-bottom: 0.5rem;
        margin-top: 0.5rem;
    }

    .capitalize { 
        text-transform: capitalize;
    }
`;

export default UpdateProfile
